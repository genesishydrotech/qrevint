import copy
import numpy as np
from qrev.Classes.Uncertainty import Uncertainty
from qrev.Classes.QComp import QComp
from qrev.Classes.MovingBedTests import MovingBedTests
from qrev.Classes.TransectData import TransectData
from qrev.MiscLibs.common_functions import cosd


class QAData(object):
    """Evaluates and stores quality assurance characteristics and messages.

    Attributes
    ----------
    q_run_threshold_caution: int
        Caution threshold for interpolated discharge for a run of invalid
        ensembles, in percent.
    q_run_threshold_warning: int
        Warning threshold for interpolated discharge for a run of invalid
        ensembles, in percent.
    q_total_threshold_caution: int
        Caution threshold for total interpolated discharge for invalid
        ensembles, in percent.
    q_total_threshold_warning: int
        Warning threshold for total interpolated discharge for invalid
        ensembles, in percent.
    transects: dict
        Dictionary of quality assurance checks for transects
    system_tst: dict
        Dictionary of quality assurance checks on the system test(s)
    compass: dict
        Dictionary of quality assurance checks on compass calibration and
        evaluations
    temperature: dict
        Dictionary of quality assurance checks on temperature comparions and
        variation
    movingbed: dict
        Dictionary of quality assurance checks on moving-bed tests
    user: dict
        Dictionary of quality assurance checks on user input data
    boat: dict
        Dictionary of quality assurance checks on boat velocities
    bt_vel: dict
        Dictionary of quality assurance checks on bottom track velocities
    gga_vel: dict
        Dictionary of quality assurance checks on gga boat velocities
    vtg_vel: dict
        Dictionary of quality assurance checks on vtg boat velocities
    w_vel: dict
        Dictionary of quality assurance checks on water track velocities
    extrapolation: dict
        Dictionary of quality assurance checks on extrapolations
    edges: dict
        Dictionary of quality assurance checks on edges
    """

    def __init__(self, meas, mat_struct=None, compute=True, tr=None):
        """Checks the measurement for all quality assurance issues.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        
        self.tr = tr

        # Set default thresholds
        self.q_run_threshold_caution = 3
        self.q_run_threshold_warning = 5
        self.q_total_threshold_caution = 10
        self.q_total_threshold_warning = 25

        # Initialize instance variables
        self.transects = dict()
        self.system_tst = dict()
        self.compass = dict()
        self.temperature = dict()
        self.movingbed = dict()
        self.user = dict()
        self.depths = dict()
        self.boat = dict()
        self.bt_vel = dict()
        self.gga_vel = dict()
        self.vtg_vel = dict()
        self.w_vel = dict()
        self.extrapolation = dict()
        self.edges = dict()
        self.settings_dict = dict()
        self.settings_dict["tab_compass"] = "Default"
        self.settings_dict["tab_tempsal"] = "Default"
        self.settings_dict["tab_mbt"] = "Default"
        self.settings_dict["tab_bt"] = "Default"
        self.settings_dict["tab_gps"] = "Default"
        self.settings_dict["tab_depth"] = "Default"
        self.settings_dict["tab_wt"] = "Default"
        self.settings_dict["tab_extrap"] = "Default"
        self.settings_dict["tab_edges"] = "Default"

        if compute:
            # Apply QA checks
            self.transects_qa(meas)
            self.system_tst_qa(meas)
            self.compass_qa(meas)
            self.temperature_qa(meas)
            self.moving_bed_qa(meas)
            self.user_qa(meas)
            self.depths_qa(meas)
            self.boat_qa(meas)
            self.water_qa(meas)
            self.extrapolation_qa(meas)
            self.edges_qa(meas)
            self.check_bt_setting(meas)
            self.check_wt_settings(meas)
            self.check_depth_settings(meas)
            self.check_gps_settings(meas)
            self.check_edge_settings(meas)
            self.check_extrap_settings(meas)
            self.check_tempsal_settings(meas)
            self.check_compass_settings(meas)
            if meas.oursin is not None:
                self.check_oursin(meas)
        else:
            self.populate_from_qrev_mat(meas, mat_struct)

    

    def populate_from_qrev_mat(self, meas, meas_struct):
        """Populates the object using data from previously saved QRev Matlab
        file.

        Parameters
        ----------
        meas: Measurement
            Object of Measurement
        meas_struct: mat_struct
           Matlab data structure obtained from sio.loadmat
        """

        # Generate a new QA object using the measurement data and the
        # current QA code. When QA checks from the current QA are not
        # available from old QRev files, these
        # checks will be included to supplement the old QRev file data.
        new_qa = QAData(meas, tr=meas.tr)
        if hasattr(meas_struct, "qa"):
            # Set default thresholds
            self.q_run_threshold_caution = meas_struct.qa.qRunThresholdCaution
            self.q_run_threshold_warning = meas_struct.qa.qRunThresholdWarning
            if hasattr(meas_struct.qa, "qTotalThresholdCaution"):
                self.q_total_threshold_caution = meas_struct.qa.qTotalThresholdCaution
            else:
                self.q_total_threshold_caution = 10
            self.q_total_threshold_warning = meas_struct.qa.qTotalThresholdWarning

            if self.q_total_threshold_warning < self.q_total_threshold_caution:
                temp = np.copy(self.q_total_threshold_warning)
                self.q_total_threshold_warning = np.copy(self.q_total_threshold_caution)
                self.q_total_threshold_caution = temp

            # Initialize instance variables
            self.transects = dict()
            self.transects["duration"] = meas_struct.qa.transects.duration
            self.transects["messages"] = self.make_list(
                meas_struct.qa.transects.messages
            )
            if hasattr(meas_struct.qa.transects, "guidance"):
                self.transects["guidance"] = self.make_list(
                    meas_struct.qa.transects.guidance)
            else:
                self.transects["guidance"] = []
            self.transects["number"] = meas_struct.qa.transects.number
            self.transects["recip"] = meas_struct.qa.transects.recip
            self.transects["sign"] = meas_struct.qa.transects.sign
            self.transects["status"] = meas_struct.qa.transects.status
            self.transects["uncertainty"] = meas_struct.qa.transects.uncertainty
            self.system_tst = dict()
            self.system_tst["messages"] = self.make_list(
                meas_struct.qa.systemTest.messages
            )
            if hasattr(meas_struct.qa.systemTest, "guidance"):
                self.system_tst["guidance"] = self.make_list(
                    meas_struct.qa.systemTest.guidance)
            else:
                self.system_tst["guidance"] = []
            self.system_tst["status"] = meas_struct.qa.systemTest.status
            self.compass = dict()
            self.compass["messages"] = self.make_list(meas_struct.qa.compass.messages)
            if hasattr(meas_struct.qa.compass, "guidance"):
                self.compass["guidance"] = self.make_list(
                    meas_struct.qa.compass.guidance)
            else:
                self.compass["guidance"] = []
            self.compass["status"] = meas_struct.qa.compass.status
            if hasattr(meas_struct.qa.compass, "status1"):
                self.compass["status1"] = meas_struct.qa.compass.status1
                self.compass["status2"] = meas_struct.qa.compass.status2
            else:
                self.compass["status1"] = "good"
                self.compass["status2"] = "good"

            # If QA check not available, get check from new QA
            if hasattr(meas_struct.qa.compass, "magvar"):
                self.compass["magvar"] = meas_struct.qa.compass.magvar
            elif "magvar" in new_qa.compass:
                self.compass["magvar"] = new_qa.compass["magvar"]
                self.compass["status"] = new_qa.compass["status"]

            # If QA check not available, get check from new QA
            if hasattr(meas_struct.qa.compass, "magvarIdx"):
                self.compass["magvar_idx"] = self.make_array(
                    meas_struct.qa.compass.magvarIdx
                )
            elif "magvar_idx" in new_qa.compass:
                self.compass["magvar_idx"] = new_qa.compass["magvar_idx"]
                self.compass["status"] = new_qa.compass["status"]

            # If QA check not available, get check from new QA
            if hasattr(meas_struct.qa.compass, "mag_error_idx"):
                self.compass["mag_error_idx"] = meas_struct.qa.compass.mag_error_idx
            elif "mag_error_idx" in new_qa.compass:
                self.compass["mag_error_idx"] = new_qa.compass["mag_error_idx"]
                self.compass["status"] = new_qa.compass["status"]

            # If QA check not available, get check from new QA
            if hasattr(meas_struct.qa.compass, "pitchMeanWarningIdx"):
                self.compass["pitch_mean_warning_idx"] = self.make_array(
                    meas_struct.qa.compass.pitchMeanWarningIdx
                )
            elif "pitch_mean_warning_idx" in new_qa.compass:
                self.compass["pitch_mean_warning_idx"] = new_qa.compass[
                    "pitch_mean_warning_idx"
                ]
                self.compass["status"] = new_qa.compass["status"]

            # If QA check not available, get check from new QA
            if hasattr(meas_struct.qa.compass, "rollMeanWarningIdx"):
                self.compass["roll_mean_warning_idx"] = self.make_array(
                    meas_struct.qa.compass.rollMeanWarningIdx
                )
            elif "roll_mean_warning_idx" in new_qa.compass:
                self.compass["roll_mean_warning_idx"] = new_qa.compass[
                    "roll_mean_warning_idx"
                ]
                self.compass["status"] = new_qa.compass["status"]

            # If QA check not available, get check from new QA
            if hasattr(meas_struct.qa.compass, "pitchMeanCautionIdx"):
                self.compass["pitch_mean_caution_idx"] = self.make_array(
                    meas_struct.qa.compass.pitchMeanCautionIdx
                )
            elif "pitch_mean_caution_idx" in new_qa.compass:
                self.compass["pitch_mean_caution_idx"] = new_qa.compass[
                    "pitch_mean_caution_idx"
                ]
                self.compass["status"] = new_qa.compass["status"]

            # If QA check not available, get check from new QA
            if hasattr(meas_struct.qa.compass, "rollMeanCautionIdx"):
                self.compass["roll_mean_caution_idx"] = self.make_array(
                    meas_struct.qa.compass.rollMeanCautionIdx
                )
            elif "roll_mean_caution_idx" in new_qa.compass:
                self.compass["roll_mean_caution_idx"] = new_qa.compass[
                    "roll_mean_caution_idx"
                ]
                self.compass["status"] = new_qa.compass["status"]

            # If QA check not available, get check from new QA
            if hasattr(meas_struct.qa.compass, "pitchStdCautionIdx"):
                self.compass["pitch_std_caution_idx"] = self.make_array(
                    meas_struct.qa.compass.pitchStdCautionIdx
                )
            elif "pitch_std_caution_idx" in new_qa.compass:
                self.compass["pitch_std_caution_idx"] = new_qa.compass[
                    "pitch_std_caution_idx"
                ]
                self.compass["status"] = new_qa.compass["status"]

            # If QA check not available, get check from new QA
            if hasattr(meas_struct.qa.compass, "rollStdCautionIdx"):
                self.compass["roll_std_caution_idx"] = self.make_array(
                    meas_struct.qa.compass.rollStdCautionIdx
                )
            elif "roll_std_caution_idx" in new_qa.compass:
                self.compass["roll_std_caution_idx"] = new_qa.compass[
                    "roll_std_caution_idx"
                ]
                self.compass["status"] = new_qa.compass["status"]

            # If QA check not available, get check from new QA
            if hasattr(meas_struct.qa.compass, "lr_water_dir"):
                self.compass["lr_water_dir"] = meas_struct.qa.compass.lr_water_dir
            elif "lr_water_dir" in new_qa.compass:
                self.compass["lr_water_dir"] = new_qa.compass["lr_water_dir"]
                self.compass["status"] = new_qa.compass["status"]

            self.temperature = dict()
            self.temperature["messages"] = self.make_list(
                meas_struct.qa.temperature.messages
            )
            if hasattr(meas_struct.qa.temperature, "guidance"):
                self.temperature["guidance"] = self.make_list(
                    meas_struct.qa.temperature.guidance)
            else:
                self.temperature["guidance"] = []
            self.temperature["status"] = meas_struct.qa.temperature.status
            self.movingbed = dict()
            self.movingbed["messages"] = self.make_list(
                meas_struct.qa.movingbed.messages
            )
            if hasattr(meas_struct.qa.movingbed, "guidance"):
                self.movingbed["guidance"] = self.make_list(
                    meas_struct.qa.movingbed.guidance)
            else:
                self.movingbed["guidance"] = []
            self.movingbed["status"] = meas_struct.qa.movingbed.status
            self.movingbed["code"] = meas_struct.qa.movingbed.code
            self.user = dict()
            self.user["messages"] = self.make_list(meas_struct.qa.user.messages)
            if hasattr(meas_struct.qa.user, "guidance"):
                self.user["guidance"] = self.make_list(
                    meas_struct.qa.user.guidance)
            else:
                self.user["guidance"] = []
            self.user["sta_name"] = bool(meas_struct.qa.user.staName)
            self.user["sta_number"] = bool(meas_struct.qa.user.staNumber)
            if hasattr(meas_struct.qa.user, "time_zone"):
                self.user["time_zone"] = bool(meas_struct.qa.user.time_zone)
            else:
                self.user["time_zone"] = False
            self.user["status"] = meas_struct.qa.user.status

            # If QA check not available, get check from new QA
            self.depths = self.create_qa_dict(self, meas_struct.qa.depths)
            if "draft" not in self.depths:
                self.depths["draft"] = new_qa.depths["draft"]
                self.depths["status"] = new_qa.depths["status"]

            if "all_invalid" not in self.depths:
                self.depths["all_invalid"] = new_qa.depths["all_invalid"]
                self.depths["status"] = new_qa.depths["status"]

            # If QA check not available, get check from new QA
            self.bt_vel = self.create_qa_dict(self, meas_struct.qa.btVel, ndim=2)
            if "all_invalid" not in self.bt_vel:
                self.bt_vel["all_invalid"] = new_qa.bt_vel["all_invalid"]
                self.bt_vel["status"] = new_qa.bt_vel["status"]

            # If QA check not available, get check from new QA
            self.gga_vel = self.create_qa_dict(self, meas_struct.qa.ggaVel, ndim=2)
            if "all_invalid" not in self.gga_vel:
                self.gga_vel["all_invalid"] = new_qa.gga_vel["all_invalid"]
            if "lag_status" not in self.gga_vel:
                self.gga_vel["lag_status"] = new_qa.gga_vel["lag_status"]
                self.gga_vel["status"] = new_qa.gga_vel["status"]

            # If QA check not available, get check from new QA
            self.vtg_vel = self.create_qa_dict(self, meas_struct.qa.vtgVel, ndim=2)
            if "all_invalid" not in self.vtg_vel:
                self.vtg_vel["all_invalid"] = new_qa.vtg_vel["all_invalid"]
            if "lag_status" not in self.vtg_vel:
                self.vtg_vel["lag_status"] = new_qa.vtg_vel["lag_status"]
                self.vtg_vel["status"] = new_qa.vtg_vel["status"]

            # If QA check not available, get check from new QA
            self.w_vel = self.create_qa_dict(self, meas_struct.qa.wVel, ndim=2)
            if "all_invalid" not in self.w_vel:
                self.w_vel["all_invalid"] = new_qa.w_vel["all_invalid"]
                self.w_vel["status"] = new_qa.w_vel["status"]

            self.extrapolation = dict()
            self.extrapolation["messages"] = self.make_list(
                meas_struct.qa.extrapolation.messages
            )
            if hasattr(meas_struct.qa.extrapolation, "guidance"):
                self.extrapolation["guidance"] = self.make_list(
                    meas_struct.qa.extrapolation.guidance)
            else:
                self.extrapolation["guidance"] = []
            self.extrapolation["status"] = meas_struct.qa.extrapolation.status
            self.edges = dict()
            self.edges["messages"] = self.make_list(meas_struct.qa.edges.messages)
            if hasattr(meas_struct.qa.edges, "guidance"):
                self.edges["guidance"] = self.make_list(
                    meas_struct.qa.edges.guidance)
            else:
                self.edges["guidance"] = []
            self.edges["status"] = meas_struct.qa.edges.status
            self.edges["left_q"] = meas_struct.qa.edges.leftQ
            self.edges["right_q"] = meas_struct.qa.edges.rightQ
            self.edges["left_sign"] = meas_struct.qa.edges.leftSign
            self.edges["right_sign"] = meas_struct.qa.edges.rightSign
            self.edges["left_zero"] = meas_struct.qa.edges.leftzero
            self.edges["right_zero"] = meas_struct.qa.edges.rightzero
            self.edges["left_type"] = meas_struct.qa.edges.leftType
            self.edges["right_type"] = meas_struct.qa.edges.rightType

            # If QA check not available, get check from new QA
            if hasattr(meas_struct.qa.edges, "rightDistMovedIdx"):
                self.edges["right_dist_moved_idx"] = self.make_array(
                    meas_struct.qa.edges.rightDistMovedIdx
                )
            else:
                self.edges["right_dist_moved_idx"] = new_qa.edges[
                    "right_dist_moved_idx"
                ]
                self.edges["status"] = new_qa.edges["status"]

            # If QA check not available, get check from new QA
            if hasattr(meas_struct.qa.edges, "leftDistMovedIdx"):
                self.edges["left_dist_moved_idx"] = self.make_array(
                    meas_struct.qa.edges.leftDistMovedIdx
                )
            else:
                self.edges["left_dist_moved_idx"] = new_qa.edges["left_dist_moved_idx"]
                self.edges["status"] = new_qa.edges["status"]

            # If QA check not available, get check from new QA
            if hasattr(meas_struct.qa.edges, "leftQIdx"):
                self.edges["left_q_idx"] = self.make_array(
                    meas_struct.qa.edges.leftQIdx
                )
            else:
                self.edges["left_q_idx"] = new_qa.edges["left_q_idx"]
                self.edges["status"] = new_qa.edges["status"]

            # If QA check not available, get check from new QA
            if hasattr(meas_struct.qa.edges, "rightQIdx"):
                self.edges["right_q_idx"] = self.make_array(
                    meas_struct.qa.edges.rightQIdx
                )
            else:
                self.edges["right_q_idx"] = new_qa.edges["right_q_idx"]
                self.edges["status"] = new_qa.edges["status"]

            # If QA check not available, get check from new QA
            if hasattr(meas_struct.qa.edges, "leftZeroIdx"):
                self.edges["left_zero_idx"] = self.make_array(
                    meas_struct.qa.edges.leftZeroIdx
                )
            else:
                self.edges["left_zero_idx"] = new_qa.edges["left_zero_idx"]
                self.edges["status"] = new_qa.edges["status"]

            # If QA check not available, get check from new QA
            if hasattr(meas_struct.qa.edges, "rightZeroIdx"):
                self.edges["right_zero_idx"] = self.make_array(
                    meas_struct.qa.edges.rightZeroIdx
                )
            else:
                self.edges["right_zero_idx"] = new_qa.edges["right_zero_idx"]
                self.edges["status"] = new_qa.edges["status"]

            # If QA check not available, get check from new QA
            if hasattr(meas_struct.qa.edges, "invalid_transect_left_idx"):
                self.edges["invalid_transect_left_idx"] = self.make_array(
                    meas_struct.qa.edges.invalid_transect_left_idx
                )
            elif hasattr(meas_struct.qa.edges, "invalidTransLeftIdx"):
                self.edges["invalid_transect_left_idx"] = self.make_array(
                    meas_struct.qa.edges.invalidTransLeftIdx
                )
            else:
                self.edges["invalid_transect_left_idx"] = new_qa.edges[
                    "invalid_transect_left_idx"
                ]
                self.edges["status"] = new_qa.edges["status"]

            # If QA check not available, get check from new QA
            if hasattr(meas_struct.qa.edges, "invalid_transect_right_idx"):
                self.edges["invalid_transect_right_idx"] = self.make_array(
                    meas_struct.qa.edges.invalid_transect_right_idx
                )
            elif hasattr(meas_struct.qa, "invalidTransRightIdx"):
                self.edges["invalid_transect_right_idx"] = self.make_array(
                    meas_struct.qa.edges.invalidTransRightIdx
                )
            else:
                self.edges["invalid_transect_right_idx"] = new_qa.edges[
                    "invalid_transect_right_idx"
                ]
                self.edges["status"] = new_qa.edges["status"]

            if hasattr(meas_struct.qa, "settings_dict"):
                self.settings_dict = dict()
                try:
                    self.settings_dict[
                        "tab_compass"
                    ] = meas_struct.qa.settings_dict.tab_compass
                except AttributeError:
                    self.settings_dict["tab_compass"] = new_qa.settings_dict[
                        "tab_compass"
                    ]

                try:
                    self.settings_dict[
                        "tab_tempsal"
                    ] = meas_struct.qa.settings_dict.tab_tempsal
                except AttributeError:
                    self.settings_dict["tab_tempsal"] = new_qa.settings_dict[
                        "tab_tempsal"
                    ]

                try:
                    self.settings_dict["tab_mbt"] = meas_struct.qa.settings_dict.tab_mbt
                except AttributeError:
                    self.settings_dict["tab_mbt"] = new_qa.settings_dict["tab_mbt"]

                try:
                    self.settings_dict["tab_bt"] = meas_struct.qa.settings_dict.tab_bt
                except AttributeError:
                    self.settings_dict["tab_bt"] = new_qa.settings_dict["tab_bt"]

                try:
                    self.settings_dict["tab_gps"] = meas_struct.qa.settings_dict.tab_gps
                except AttributeError:
                    self.settings_dict["tab_gps"] = new_qa.settings_dict["tab_gps"]

                try:
                    self.settings_dict[
                        "tab_depth"
                    ] = meas_struct.qa.settings_dict.tab_depth
                except AttributeError:
                    self.settings_dict["tab_depth"] = new_qa.settings_dict["tab_depth"]

                try:
                    self.settings_dict["tab_wt"] = meas_struct.qa.settings_dict.tab_wt
                except AttributeError:
                    self.settings_dict["tab_wt"] = new_qa.settings_dict["tab_wt"]

                try:
                    self.settings_dict[
                        "tab_extrap"
                    ] = meas_struct.qa.settings_dict.tab_extrap
                except AttributeError:
                    self.settings_dict["tab_extrap"] = new_qa.settings_dict[
                        "tab_extrap"
                    ]

                try:
                    self.settings_dict[
                        "tab_edges"
                    ] = meas_struct.qa.settings_dict.tab_edges
                except AttributeError:
                    self.settings_dict["tab_edges"] = new_qa.settings_dict["tab_edges"]

    @staticmethod
    def create_qa_dict(self, mat_data, ndim=1):
        """Creates the dictionary used to store QA checks associated with
        the percent of discharge estimated by interpolation. This dictionary
        is used by BT, GPS, Depth, and WT.

        Parameters
        ----------
        self: QAData
            Object of QAData
        mat_data: mat_struct
            Matlab data from QRev file
        ndim: int
            Number of dimensions in data
        """

        # Initialize dictionary
        qa_dict = dict()

        # Populate dictionary from Matlab data
        qa_dict["messages"] = QAData.make_list(mat_data.messages)
        if hasattr(mat_data, "guidance"):
            qa_dict["guidance"] = self.make_list(mat_data.guidance)
        else:
            qa_dict["guidance"] = []

        # allInvalid not available in older QRev data
        if hasattr(mat_data, "allInvalid"):
            qa_dict["all_invalid"] = self.make_array(mat_data.allInvalid, 1).astype(
                bool
            )

        qa_dict["q_max_run_caution"] = self.make_array(
            mat_data.qRunCaution, ndim
        ).astype(bool)
        qa_dict["q_max_run_warning"] = self.make_array(
            mat_data.qRunWarning, ndim
        ).astype(bool)
        if hasattr(mat_data, "qTotalCaution"):
            qa_dict["q_total_caution"] = self.make_array(
                mat_data.qTotalCaution, ndim
            ).astype(bool)
        else:
            qa_dict["q_total_caution"] = self.make_array(
                mat_data.qTotalWarning, ndim
            ).astype(bool)
        qa_dict["q_total_warning"] = self.make_array(
            mat_data.qTotalWarning, ndim
        ).astype(bool)
        qa_dict["status"] = mat_data.status

        # q_max_run and q_total not available in older QRev data
        try:
            qa_dict["q_max_run"] = self.make_array(mat_data.qMaxRun, ndim)
            qa_dict["q_total"] = self.make_array(mat_data.qTotal, ndim)
        except AttributeError:
            qa_dict["q_max_run"] = np.tile(np.nan, (len(mat_data.qRunCaution), 6))
            qa_dict["q_total"] = np.tile(np.nan, (len(mat_data.qRunCaution), 6))
        return qa_dict

    @staticmethod
    def make_array(num_in, ndim=1):
        """Ensures that num_in is an array and if not makes it an array.

        num_in: any
            Any value or array
        """

        if type(num_in) is np.ndarray:
            if len(num_in.shape) < 2 and ndim > 1:
                num_in = np.reshape(num_in, (1, num_in.shape[0]))
                return num_in
            else:
                return num_in
        else:
            return np.array([num_in])

    @staticmethod
    def make_list(array_in):
        """Converts a string or array to a list.

        Parameters
        ----------
        array_in: any
            Data to be converted to list.

        Returns
        -------
        list_out: list
            List of array_in data
        """

        list_out = []
        # Convert string to list
        if type(array_in) is str:
            list_out = [array_in]
        else:
            # Empty array
            if array_in.size == 0:
                list_out = []
            # Single message with integer codes at end
            elif array_in.size == 3:
                if not type(array_in[1]) is np.ndarray:
                    if type(array_in[1]) is int or len(array_in[1].strip()) == 1:
                        temp = array_in.tolist()
                        if len(temp) > 0:
                            internal_list = []
                            for item in temp:
                                internal_list.append(item)
                            list_out = [internal_list]
                    else:
                        list_out = array_in.tolist()
                else:
                    list_out = array_in.tolist()
            # Either multiple messages with or without integer codes
            else:
                list_out = array_in.tolist()

        return list_out

    @staticmethod
    def guidance_prep(message_text, guidance_text):
        return message_text + "\n" + " -- " + guidance_text + "\n"

    def transects_qa(self, meas):
        """Apply quality checks to transects

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        # Assume good results
        self.transects["status"] = "good"

        # Initialize keys
        self.transects["messages"] = []
        self.transects["guidance"] = []
        self.transects["recip"] = 0
        self.transects["sign"] = 0
        self.transects["duration"] = 0
        self.transects["number"] = 0
        self.transects["uncertainty"] = 0
        self.transects["batt_voltage"] = []
        # Battery voltage
        batt_threshold = 10.5

        # Initialize lists
        checked = []
        discharges = []
        start_edge = []

        # Populate lists
        for n in range(len(meas.transects)):
            checked.append(meas.transects[n].checked)
            if meas.transects[n].checked:
                discharges.append(meas.discharge[n])
                start_edge.append(meas.transects[n].start_edge)

        num_checked = np.nansum(np.asarray(checked))

        # Check minimum number of transects
        if num_checked < meas.min_transects:
            self.transects["status"] = "caution"
            text = (self.tr(
                "Transects: The number of selected transects is less than ")
                + str(meas.min_transects)
                + ";"
            )
            self.transects["messages"].append([text, 2, 0])
            guidance_text = self.tr("Collect additional transects until the total number of selected transects meets or exceeds the agency minimum recommended number and transects are reciprocal transects. If this is not possible, provide a comment explaining the situation.")
            self.transects["guidance"].append(
                self.guidance_prep(self.transects["messages"][-1][0], guidance_text)
            )
            self.transects["duration"] = 1

        # Check duration
        total_duration = 0
        if num_checked >= 1:
            for transect in meas.transects:
                if transect.checked:
                    total_duration += transect.date_time.transect_duration_sec

        # Check duration against policy
        if total_duration < meas.min_duration:
            self.transects["status"] = "caution"
            text = (
                self.tr("Transects: Duration of selected transects is less than ")
                + str(meas.min_duration)
                + self.tr(" seconds") + ";"
            )
            self.transects["messages"].append([text, 2, 0])
            self.transects["duration"] = 1
            guidance_text = self.tr("Reduce the boat speed and/or collect additional transects until the total duration exceeds the agency minimum recommended duration and transects are reciprocal transects. If this is not possible, provide a comment explaining the situation.")
            self.transects["guidance"].append(
                self.guidance_prep(self.transects["messages"][-1][0], guidance_text)
            )

        # Check transects for missing ensembles
        left_invalid_exceeded = False
        right_invalid_exceeded = False
        for transect in meas.transects:
            if transect.checked:
                # Determine number of missing ensembles
                if transect.adcp.manufacturer == "SonTek":
                    # Determine number of missing ensembles for SonTek data
                    idx_missing = np.where(transect.date_time.ens_duration_sec > 1.5)[0]
                    if len(idx_missing) > 0:
                        average_ensemble_duration = (
                            np.nansum(transect.date_time.ens_duration_sec)
                            - np.nansum(
                                transect.date_time.ens_duration_sec[idx_missing]
                            )
                        ) / (
                            len(transect.date_time.ens_duration_sec) - len(idx_missing)
                        )
                        num_missing = np.round(
                            np.nansum(transect.date_time.ens_duration_sec[idx_missing])
                            / average_ensemble_duration
                        ) - len(idx_missing)
                    else:
                        num_missing = 0
                else:
                    # Determine number of lost ensembles for TRDI data
                    idx_missing = np.where(
                        np.isnan(transect.date_time.ens_duration_sec)
                    )[0]
                    num_missing = len(idx_missing) - 1

                # Save caution message
                if num_missing > 0:
                    self.transects["messages"].append(
                        [
                            self.tr("Transects: ")
                            + str(transect.file_name)
                            + self.tr(" is missing ")
                            + str(int(num_missing))
                            + self.tr(" ensembles") + ";",
                            2,
                            0,
                        ]
                    )
                    self.transects["status"] = "caution"
                    guidance_text = self.tr("Missing ensembles are typically due to communication problems between the instrument and the computer.  If the missing ensembles occur randomly and infrequently the measurement is likely unaffected by them. However, if the number of missing ensembles may affect the final discharge consider recollecting the data with a different computer/serial port/wireless communications/etc. If this measurement was made with an M9 or S5 download the data from the ADCP and use those data instead of the data stored on the computer by RiverSurveyor Live or RSQ.")
                    self.transects["guidance"].append(
                        self.guidance_prep(
                            self.transects["messages"][-1][0], guidance_text
                        )
                    )

                # Invalid ensembles at left and/or right edge
                boat_selected = getattr(transect.boat_vel, transect.boat_vel.selected)
                if boat_selected is None:
                    valid_bt = np.tile(False, transect.boat_vel.bt_vel.u_mps.size)
                else:
                    valid_bt = boat_selected.valid_data[0, :]
                valid_wt = np.any(transect.w_vel.valid_data[0, :, :], axis=0)
                depth_selected = getattr(transect.depths, transect.depths.selected)
                valid_depth = depth_selected.valid_data
                valid_all = np.vstack([valid_bt, valid_wt, valid_depth])
                valid = np.all(valid_all, axis=0)

                if transect.sensors is not None:
                    if hasattr(transect.sensors, "battery_voltage"):
                        if transect.sensors.battery_voltage.internal is not None:
                            if transect.adcp.model == "RS5":
                                batt_threshold = 3.3

                            if (
                                np.nanmin(
                                    transect.sensors.battery_voltage.internal.data
                                )
                                < batt_threshold
                            ):
                                self.transects["batt_voltage"].append(
                                    transect.file_name[:-4]
                                )

        # Message for low battery
        if len(self.transects["batt_voltage"]) > 0:
            self.transects["status"] = "caution"
            text = (
                self.tr("Transects: ")
                + str(self.transects["batt_voltage"])
                + self.tr(" have battery voltage less than ")
                + str(batt_threshold)
            )
            self.transects["messages"].append([text, 2, 0])
            guidance_text = self.tr("Low battery voltage may cause range issues with some ADCPs in some conditions. Evaluate the data carefully to ensure the data appear correct. If in the field, use a charged battery to recollect the data, if necessary.")
            self.transects["guidance"].append(
                self.guidance_prep(self.transects["messages"][-1][0], guidance_text)
            )

        # Check number of transects checked
        if num_checked == 0:
            # No transects selected
            self.transects["status"] = "warning"
            self.transects["messages"].append(
                [self.tr("TRANSECTS: No transects selected") +";", 1, 0]
            )
            self.transects["number"] = 2
            guidance_text = self.tr("For reasonably steady flow check a sufficient number or reciprocal transects to achieve agency recommended minimum duration and number or reciprocal transects. For rapidly varying flow check an appropriate number of transects while trying to maintain reciprocal transects.")
            self.transects["guidance"].append(
                self.guidance_prep(self.transects["messages"][-1][0], guidance_text)
            )

        elif num_checked == 1:
            # Only one transect selected
            self.transects["status"] = "caution"
            self.transects["messages"].append(
                [self.tr("Transects: Only one transect selected") + ";", 2, 0]
            )
            self.transects["number"] = 2
            guidance_text = self.tr("Reciprocal transects are recommended to avoid potential directional bias. If flow is changing too rapidly for reciprocal transects add a comment to document the situation.")
            self.transects["guidance"].append(
                self.guidance_prep(self.transects["messages"][-1][0], guidance_text)
            )

        else:
            self.transects["number"] = num_checked
            if num_checked == 2:
                # Only 2 transects selected
                cov, _ = Uncertainty.uncertainty_q_random(discharges, "total")
                # Check uncertainty
                if cov > 2:
                    self.transects["status"] = "caution"
                    self.transects["messages"].append(
                        [
                            self.tr("Transects: Uncertainty would be reduced by additional transects") +
                            ";",
                            2,
                            0,
                        ]
                    )
                    guidance_text = self.tr("Collecting additional reciprocal transects would reduce the random uncertainty associated with this measurement assuming near steady flow conditions.")
                    self.transects["guidance"].append(
                        self.guidance_prep(
                            self.transects["messages"][-1][0], guidance_text
                        )
                    )

            if num_checked < meas.min_transects:
                self.transects["status"] = "caution"
                text = (
                    self.tr("Transects: Number of transects is below the required minimum of ")
                    + str(meas.min_transects) + ";"
                )
                self.transects["messages"].append([text, 2, 0])
                guidance_text = self.tr("Unless the flow is changing rapidly, collect additional transect to meet the agency minimum requirement. If conditions do not allow collection of additional transects, document the situation.")
                self.transects["guidance"].append(
                    self.guidance_prep(self.transects["messages"][-1][0], guidance_text)
                )

            # Check for consistent sign
            q_positive = []
            for q in discharges:
                if q.total >= 0:
                    q_positive.append(True)
                else:
                    q_positive.append(False)
            if len(np.unique(q_positive)) > 1:
                self.transects["status"] = "warning"
                self.transects["messages"].append(
                    [
                        self.tr("TRANSECTS: Sign of total Q is not consistent. One or more start banks may be incorrect") + ";",
                        1,
                        0,
                    ]
                )
                guidance_text = self.tr("Check the start bank for each transect. If the start banks are correct and the flow is rapidly changing to a reverse flow condition, consider breaking the measurement into multiple measurements to represent the conditions.")
                self.transects["guidance"].append(
                    self.guidance_prep(self.transects["messages"][-1][0], guidance_text)
                )

            # Check for reciprocal transects
            num_left = start_edge.count("Left")
            num_right = start_edge.count("Right")

            if not num_left == num_right:
                self.transects["status"] = "warning"
                self.transects["messages"].append(
                    [
                        self.tr("TRANSECTS: Transects selected are not reciprocal transects") + ";",
                        1,
                        0,
                    ]
                )
                guidance_text = self.tr("Unless conditions require use of a single transect, transects should be collected in reciprocal pairs to reduce potential directional bias. Consider adding or removing a transect from the measurement to achieve reciprocal transects.")
                self.transects["guidance"].append(
                    self.guidance_prep(self.transects["messages"][-1][0], guidance_text)
                )

        # Check for zero discharge transects
        q_zero = False
        for q in discharges:
            if q.total == 0:
                q_zero = True
        if q_zero:
            self.transects["status"] = "warning"
            self.transects["messages"].append(
                [self.tr("TRANSECTS: One or more transects have zero Q") + ";", 1, 0]
            )
            guidance_text = self.tr("A zero discharge usually occurs when all ensembles have invalid depth, boat speed, or water speed. Changing the depth or boat reference may help. Otherwise the transect should not be included in the final discharge computation.")
            self.transects["guidance"].append(
                self.guidance_prep(self.transects["messages"][-1][0], guidance_text)
            )

    def system_tst_qa(self, meas):
        """Apply QA checks to system test.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        self.system_tst["messages"] = []
        self.system_tst["status"] = "good"
        self.system_tst["guidance"] = []

        if "tab_systst" not in self.settings_dict:
            self.settings_dict["tab_systst"] = "Default"

        # Determine if a system test was recorded
        if not meas.system_tst:
            # No system test data recorded
            self.system_tst["status"] = "warning"
            self.system_tst["messages"].append([self.tr("SYSTEM TEST: No system test") + ";", 1, 3])
            guidance_text = self.tr("A system test is recommended to be completed prior to every discharge measurement to ensure the ADCP is operating properly. If still in the field, complete a system test.")
            self.system_tst["guidance"].append(
                self.guidance_prep(self.system_tst["messages"][-1][0], guidance_text)
            )

        else:
            pt3_fail = False
            num_tests_with_failure = 0

            for test in meas.system_tst:
                if hasattr(test, "result"):
                    # Check for presence of pt3 test
                    if "pt3" in test.result and test.result["pt3"] is not None:
                        # Check hard_limit, high gain, wide bandwidth
                        if "hard_limit" in test.result["pt3"]:
                            if "high_wide" in test.result["pt3"]["hard_limit"]:
                                corr_table = test.result["pt3"]["hard_limit"][
                                    "high_wide"
                                ]["corr_table"]
                                if len(corr_table) > 0:
                                    # All lags past lag 2 should be less
                                    # than 50% of lag 0
                                    qa_threshold = corr_table[0, :] * 0.5
                                    all_lag_check = np.greater(
                                        corr_table[3::, :], qa_threshold
                                    )

                                    # Lag 7 should be less than 25% of lag 0
                                    lag_7_check = np.greater(
                                        corr_table[7, :], corr_table[0, :] * 0.25
                                    )

                                    # If either condition is met for any
                                    # beam the test fails
                                    if (
                                        np.sum(np.sum(all_lag_check))
                                        + np.sum(lag_7_check)
                                        > 1
                                    ):
                                        pt3_fail = True

                    if (
                        test.result["sysTest"]["n_failed"] is not None
                        and test.result["sysTest"]["n_failed"] > 0
                    ):
                        num_tests_with_failure += 1

            # pt3 test failure message
            if pt3_fail:
                self.system_tst["status"] = "caution"
                self.system_tst["messages"].append(
                    [
                        self.tr("System Test: One or more PT3 tests in the system test indicate potential EMI") + ";",
                        2,
                        3,
                    ]
                )
                guidance_text = self.tr("A failed PT3 test indicates there is potential electromagnetic interference. Errors in measured velocities caused by EMI tend to be a consistent bias (not related to true water velocity), so errors will be a greater percentage in lower velocities. EMI is more likely to occur on a StreamPro ADCP. To determine if EMI is affecting the measurement: 1) look for unusual patterns in the measured velocities, such as, higher velocities near the streambed, 2) use the Adv Graph tab and plot the average water track correlation contour plot and look for an increase in correlation with depth, 3) use the Adv Graph tab and plot the water track vertical velocity and look for a vertical pattern, such as, increasing negative or positive velocities towards the surface or streambed (a normal vertical velocity contour plot should look more random without vertical patterns). If any of these conditions are observed the measurement is affected and a different measurement site should be selected or the measurement at this site should be made with a different instrument.")
                self.system_tst["guidance"].append(
                    self.guidance_prep(
                        self.system_tst["messages"][-1][0], guidance_text
                    )
                )

            # Check for failed tests
            if num_tests_with_failure == len(meas.system_tst):
                # All tests had a failure
                self.system_tst["status"] = "warning"
                self.system_tst["messages"].append(
                    [
                        self.tr("SYSTEM TEST: All system test sets have at least one test that failed") + ";",
                        1,
                        3,
                    ]
                )
                guidance_text = self.tr("If a system test fails, try repeating the test in calm water. If failures continue, proceed with the measurement and monitor the data closely. If the data appear valid, the measurement is probably OK. However, if this ADCP continues to fail system tests at other sites, the ADCP should be evaluated and potentially sent to the manufacturer for their evaluation and repair.")
                self.system_tst["guidance"].append(
                    self.guidance_prep(
                        self.system_tst["messages"][-1][0], guidance_text
                    )
                )

            elif num_tests_with_failure > 0:
                self.system_tst["status"] = "caution"
                self.system_tst["messages"].append(
                    [
                        self.tr("System Test: One or more system test sets have at least one test that failed") + ";",
                        2,
                        3,
                    ]
                )
                guidance_text = self.tr("If a system test fails, try repeating the test in calm water. If at least one system test passed, the system is likely working properly. Always proceed with the measurement and monitor the data closely. If the data appear valid, the measurement is probably valid.")
                self.system_tst["guidance"].append(
                    self.guidance_prep(
                        self.system_tst["messages"][-1][0], guidance_text
                    )
                )

        # Check for a custom transformation matrix
        for transect in meas.transects:
            if transect.checked:
                if transect.adcp.t_matrix.source == "Nominal":
                    # This secondary check is necessary due to an earlier QRev bug
                    # that indicated the source as Nominal even though it was a
                    # custom matrix obtained from the ADCP. Thus loading a measurement
                    # saved from a earlier version could create a false alert.
                    # RiverRay matrices are always a standard matrix based on 30 degree
                    # beams, but other TRDI ADCPs should be different from their
                    # standard matrix based on 20 degree beams.
                    # This secondary check is not necessary for Sontek ADCPs.
                    if (
                        transect.adcp.model != "RiverRay"
                        and transect.adcp.manufacturer == "TRDI"
                    ):
                        nominal_matrix = [
                            [1.4619, -1.4619, 0, 0],
                            [0, 0, -1.4619, 1.4619],
                            [0.2661, 0.2661, 0.2661, 0.2661],
                            [1.0337, 1.0337, -1.0337, -1.0337],
                        ]
                        if np.allclose(nominal_matrix, transect.adcp.t_matrix.matrix):
                            self.system_tst["status"] = "caution"
                            self.system_tst["messages"].append(
                                [
                                    self.tr("System Test: ADCP is using a nominal matrix rather than a custom matrix") + ";",
                                    2,
                                    3,
                                ]
                            )
                            guidance_text = self.tr("Most ADCPs have a custom transformation matrix (except for the RiverRay). If this ADCP has a nominal matrix, check the instruments history log to determine if it ever had a custom matrix. It may also be appropriate to contact the manufacturer to determine the transformation matrix for that ADCP serial number.")
                            self.system_tst["guidance"].append(
                                self.guidance_prep(
                                    self.system_tst["messages"][-1][0], guidance_text
                                )
                            )

                            break

    def compass_qa(self, meas):
        """Apply QA checks to compass calibration and evaluation.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        # Initialize variables
        self.compass["messages"] = []
        self.compass["status"] = "inactive"
        self.compass["status1"] = "good"
        self.compass["status2"] = "good"
        self.compass["lr_water_dir"] = "good"
        self.compass["guidance"] = []

        # Check to see if measurement has compass data
        if not self.compass_qa_has_compass(meas):
            return

        # Initialize variables
        self.compass["magvar"] = 0
        self.compass["magvar_idx"] = []
        self.compass["mag_error_idx"] = []
        self.compass["pitch_mean_warning_idx"] = []
        self.compass["pitch_mean_caution_idx"] = []
        self.compass["pitch_std_caution_idx"] = []
        self.compass["roll_mean_warning_idx"] = []
        self.compass["roll_mean_caution_idx"] = []
        self.compass["roll_std_caution_idx"] = []

        # Check calibration and evaluation of compass
        magvar_required = self.compass_qa_calibration(meas)

        # Compute data to check heading, pitch, and roll
        hpr = self.compass_qa_hpr(meas)

        # Check magvar consistency
        if len(np.unique(hpr["magvar"])) > 1:
            self.compass["status2"] = "caution"
            self.compass["messages"].append(
                [self.tr("Compass: Magnetic variation is not consistent among transects") + ";", 2, 4]
            )
            self.compass["magvar"] = 1
            guidance_text = self.tr("The magnetic variation is site dependent and should be the same for all transects in a measurement. The magnetic variation should not be changed to account for compass errors. Using an app on your phone, site information, and/or an internet search enter and appropriate magnetic variation for this site.")
            self.compass["guidance"].append(
                self.guidance_prep(self.compass["messages"][-1][0], guidance_text)
            )

        # Check heading offset consistency
        if len(np.unique(hpr["align"])) > 1:
            self.compass["status2"] = "caution"
            self.compass["messages"].append(
                [self.tr("Compass: Heading offset is not consistent among transects") + ";", 2, 4]
            )
            self.compass["align"] = 1
            guidance_text = self.tr("The heading offset is the offset in degrees between an external compass and the ADCP heading reference point. This should be consistent for the measurement unless the external compass orientation was changed during the measurement. The heading offset is normally obtained by collecting transects in the upstream and downstream directions and evaluating the GC-BC.")
            self.compass["guidance"].append(
                self.guidance_prep(self.compass["messages"][-1][0], guidance_text)
            )

        # Check that magvar was set if GPS data are available
        if magvar_required:
            if 0 in hpr["magvar"]:
                self.compass["status2"] = "warning"
                self.compass["messages"].append(
                    [self.tr("COMPASS: Magnetic variation is 0 and GPS data are present") + ";", 1, 4]
                )
                self.compass["magvar"] = 2
                self.compass["magvar_idx"] = np.where(np.array(hpr["magvar"]) == 0)[
                    0
                ].tolist()
                guidance_text = self.tr("A magnetic variation is required when GPS is used as the navigation reference. There are some locations where a zero value for magnetic variation is valid but those are very rare. The magnetic variation can be obtained for your site using a phone app or the internet. If zero is the correct value, simple enter a small value like 0.001 to avoid this message.")
                self.compass["guidance"].append(
                    self.guidance_prep(self.compass["messages"][-1][0], guidance_text)
                )

        # Check pitch mean
        if np.any(np.asarray(np.abs(hpr["pitch_mean"])) > 8):
            self.compass["status2"] = "warning"
            self.compass["messages"].append(
                [self.tr("PITCH: One or more transects have a mean pitch > 8 deg") + ";", 1, 4]
            )
            guidance_text = self.tr("A consistent pitch is usually the result of a poor mount or the upward tension on the tether of a tethered boat. Adjust the mount to reduce the pitch or add a weight onto the tether near the tether boat to reduce the pitch.")
            self.compass["guidance"].append(
                self.guidance_prep(self.compass["messages"][-1][0], guidance_text)
            )

            temp = np.where(np.abs(hpr["pitch_mean"]) > 8)[0]
            if len(temp) > 0:
                self.compass["pitch_mean_warning_idx"] = np.array(
                    meas.checked_transect_idx
                )[temp]
            else:
                self.compass["pitch_mean_warning_idx"] = []

        elif np.any(np.asarray(np.abs(hpr["pitch_mean"])) > 4):
            if self.compass["status2"] == "good":
                self.compass["status2"] = "caution"
            self.compass["messages"].append(
                [self.tr("Pitch: One or more transects have a mean pitch > 4 deg") + ";", 2, 4]
            )
            guidance_text = self.tr("A consistent pitch is usually the result of a poor mount or the upward tension on the tether of a tethered boat. Adjust the mount to reduce the pitch or add a weight onto the tether near the tether boat to reduce the pitch.")
            self.compass["guidance"].append(
                self.guidance_prep(self.compass["messages"][-1][0], guidance_text)
            )
            temp = np.where(np.abs(hpr["pitch_mean"]) > 4)[0]
            if len(temp) > 0:
                self.compass["pitch_mean_caution_idx"] = np.array(
                    meas.checked_transect_idx
                )[temp]
            else:
                self.compass["pitch_mean_caution_idx"] = []

        # Check roll mean
        if np.any(np.asarray(np.abs(hpr["roll_mean"])) > 8):
            self.compass["status2"] = "warning"
            self.compass["messages"].append(
                [self.tr("ROLL: One or more transects have a mean roll > 8 deg") + ";", 1, 4]
            )
            guidance_text = self.tr("A consistent roll is usually due to a poor mount or unevenly distributed weight on the boat (manned, tethered, or remote-control). Correct the mount or weight distribution.")
            self.compass["guidance"].append(
                self.guidance_prep(self.compass["messages"][-1][0], guidance_text)
            )
            temp = np.where(np.abs(hpr["roll_mean"]) > 8)[0]
            if len(temp) > 0:
                self.compass["roll_mean_warning_idx"] = np.array(
                    meas.checked_transect_idx
                )[temp]
            else:
                self.compass["roll_mean_warning_idx"] = []

        elif np.any(np.asarray(np.abs(hpr["roll_mean"])) > 4):
            if self.compass["status2"] == "good":
                self.compass["status2"] = "caution"
            self.compass["messages"].append(
                [self.tr("Roll: One or more transects have a mean roll > 4 deg") + ";", 2, 4]
            )
            guidance_text = self.tr("A consistent roll is usually due to a poor mount or unevenly distributed weight on the boat (manned, tethered, or remote-control). Correct the mount or weight distribution.")
            self.compass["guidance"].append(
                self.guidance_prep(self.compass["messages"][-1][0], guidance_text)
            )
            temp = np.where(np.abs(hpr["roll_mean"]) > 4)[0]
            if len(temp) > 0:
                self.compass["roll_mean_caution_idx"] = np.array(
                    meas.checked_transect_idx
                )[temp]
            else:
                self.compass["roll_mean_caution_idx"] = []

        # Check pitch standard deviation
        if np.any(np.asarray(hpr["pitch_std"]) > 5):
            if self.compass["status2"] == "good":
                self.compass["status2"] = "caution"
            self.compass["messages"].append(
                [self.tr("Pitch: One or more transects have a pitch std dev > 5 deg") + ";", 2, 4]
            )
            guidance_text = self.tr("Variable pitch can cause inaccuracies in the measured water and bottom track. To evaluate the potential effects of pitch on the collected data, use the Adv Graph tab and plot the water track speed, bottom track speed, and pitch time series and look for correlation between spikes in the water or bottom track and spikes in the pitch. If the spikes appear to make a substantial change in discharge, the quality of the measurement may need to be downgraded.")
            self.compass["guidance"].append(
                self.guidance_prep(self.compass["messages"][-1][0], guidance_text)
            )
            temp = np.where(np.abs(hpr["pitch_std"]) > 5)[0]
            if len(temp) > 0:
                self.compass["pitch_std_caution_idx"] = np.array(
                    meas.checked_transect_idx
                )[temp]
            else:
                self.compass["pitch_std_caution_idx"] = []

        # Check roll standard deviation
        if np.any(np.asarray(hpr["roll_std"]) > 5):
            if self.compass["status2"] == "good":
                self.compass["status2"] = "caution"
            self.compass["messages"].append(
                [self.tr("Roll: One or more transects have a roll std dev > 5 deg") + ";", 2, 4]
            )
            guidance_text = self.tr("Variable roll can cause inaccuracies in the measured water and bottom track. To evaluate the potential effects of roll on the collected data, use the Adv Graph tab and plot the water track speed, bottom track speed, and pitch time series and look for correlation between spikes in the water or bottom track and spikes in the roll. If the spikes appear to make a substantial change in discharge, the quality of the measurement may need to be downgraded.")
            self.compass["guidance"].append(
                self.guidance_prep(self.compass["messages"][-1][0], guidance_text)
            )
            temp = np.where(np.abs(hpr["roll_std"]) > 5)[0]
            if len(temp) > 0:
                self.compass["roll_std_caution_idx"] = np.array(
                    meas.checked_transect_idx
                )[temp]
            else:
                self.compass["roll_std_caution_idx"] = []

        # Check difference in water direction
        if magvar_required and (hpr["water_dir_diff"] > meas.water_dir_diff_threshold):
            error = cosd(hpr["water_dir_diff"])
            self.compass["lr_water_dir"] = "caution"
            self.compass["messages"].append(
                [
                    self.tr("Compass: The difference in the left and right water directions could cause an error in average Q of ") + " {:3.1f}% ;".format(error),
                    2,
                    4,
                ]
            )
            guidance_text = self.tr("An accurate heading is required for this measurement since either GPS is used or a loop moving-bed test was completed. There is a greater than expected difference in the water direction measured for transects starting on the left bank from those starting on the right bank. This difference indicates the compass is not accurate. Recalibrate the compass and recollect the data, if possible.")
            self.compass["guidance"].append(
                self.guidance_prep(self.compass["messages"][-1][0], guidance_text)
            )

        # Additional checks for SonTek G3 compass
        if meas.transects[meas.checked_transect_idx[0]].adcp.manufacturer == "SonTek":
            # Check if pitch limits were exceeded
            if any(hpr["pitch_exceeded"]):
                if self.compass["status2"] == "good":
                    self.compass["status2"] = "caution"
                self.compass["messages"].append(
                    [
                        self.tr("Compass: One or more transects have pitch exceeding calibration limits") + ";",
                        2,
                        4,
                    ]
                )
                guidance_text = self.tr("Exceeding the pitch range from the compass calibration can result in inaccurate headings. If the exceedance is small the inaccuracies are likely small. If they are large and you are in the field, recalibrate the compass using an appropriate pitch range. If in the office, look at the Compass/P/R tab and see if there is a change in heading with a change in pitch beyond the limits.")
                self.compass["guidance"].append(
                    self.guidance_prep(self.compass["messages"][-1][0], guidance_text)
                )

            # Check if roll limits were exceeded
            if any(hpr["roll_exceeded"]):
                if self.compass["status2"] == "good":
                    self.compass["status2"] = "caution"
                self.compass["messages"].append(
                    [
                        self.tr("Compass: One or more transects have roll exceeding calibration limits") + ";",
                        2,
                        4,
                    ]
                )
                guidance_text = self.tr("Exceeding the roll range from the compass calibration can result in inaccurate headings. If the exceedance is small the inaccuracies are likely small. If they are large and you are in the field, recalibrate the compass using an appropriate roll range. If in the office, look at the Compass/P/R tab and see if there is a change in heading with a change in roll beyond the limits.")
                self.compass["guidance"].append(
                    self.guidance_prep(self.compass["messages"][-1][0], guidance_text)
                )

            # Check if magnetic error was exceeded
            self.compass["mag_error_idx"] = []
            if len(hpr["mag_error_exceeded"]) > 0:
                self.compass["mag_error_idx"] = np.array(hpr["mag_error_exceeded"])
                if self.compass["status2"] == "good":
                    self.compass["status2"] = "caution"
                self.compass["messages"].append(
                    [
                        self.tr("Compass: One or more transects have a change in mag field exceeding 2%") + ";",
                        2,
                        4,
                    ]
                )
                guidance_text = self.tr("The G3 compass evaluates the strength of the magnetic field during calibration and during collection of transects. A change in magnetic field greater than 2% during a transect indicates the magnetic field has change from that measured during the compass calibration due to magnetic interference. Using the Compass/P/R tab look at the heading time series plot for changes in heading that correlate with changes in the magnetic field. If the interference is substantial consider moving up or down stream away from the source of the interference.")
                self.compass["guidance"].append(
                    self.guidance_prep(self.compass["messages"][-1][0], guidance_text)
                )

        # Determine status of compass tab
        if self.compass["status1"] == "warning" or self.compass["status2"] == "warning":
            self.compass["status"] = "warning"
        elif (
            self.compass["status1"] == "caution"
            or self.compass["status2"] == "caution"
            or self.compass["lr_water_dir"] == "caution"
        ):
            self.compass["status"] = "caution"
        else:
            self.compass["status"] = "good"

    def compass_qa_has_compass(self, meas):
        """Determine if the data has heading data and thus a compass.

         Parameters
        ----------
        meas: Measurement
            Object of class Measurement

        Returns
        -------
        True or False
        """

        if len(meas.checked_transect_idx) > 0:
            heading = np.unique(
                meas.transects[
                    meas.checked_transect_idx[0]
                ].sensors.heading_deg.internal.data
            )
            if len(heading) > 1:
                return True

        return False

    def compass_qa_calibration(self, meas):
        """Determine if a compass calibration is required and if they have been
        completed appropriately. A compass calibration is required if a loop test
        or GPS are used.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement

        Returns
        -------
        gps: bool
            Indicates that gps is used and magvar is required.
        """

        # Check for loop test
        loop = False
        for test in meas.mb_tests:
            if test.type == "Loop":
                loop = True
                break

        # Check for GPS data
        gps = False
        for idx in meas.checked_transect_idx:
            if (
                meas.transects[idx].boat_vel.gga_vel is not None
                or meas.transects[idx].boat_vel.vtg_vel is not None
            ):
                gps = True
                break

        if gps or loop:
            # Calibration required
            if (
                meas.transects[meas.checked_transect_idx[0]].adcp.manufacturer
                == "SonTek"
            ):
                self.compass_qa_sontek_cal(meas)

            elif (
                meas.transects[meas.checked_transect_idx[0]].adcp.manufacturer == "TRDI"
            ):
                self.compass_qa_trdi_caleval(meas)

        else:
            # Compass not required
            if len(meas.compass_cal) == 0 and len(meas.compass_eval) == 0:
                # No compass calibration or evaluation
                self.compass["status1"] = "default"
            else:
                # Compass was calibrated and evaluated
                self.compass["status1"] = "good"

        return gps

    def compass_qa_sontek_cal(self, meas):
        """Evaluate compass calibration for SonTek ADCP.

         Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        # SonTek ADCP
        if len(meas.compass_cal) == 0:
            # No compass calibration
            self.compass["status1"] = "warning"
            self.compass["messages"].append([self.tr("COMPASS: No compass calibration") + ";", 1, 4])
            guidance_text = self.tr("Using GPS as the navigation reference and conducting a loop moving-bed test require an accurate compass. Substantial errors can occur if the compass is not accurate. If in the field, calibrated the compass and recollect the data. If in the office, carefully evaluate the measurement. If GPS is used as the navigation reference, use the shiptrack plot and compare the angle between the GPS data and the BT data for reciprocal transects (turning off the vectors may help). The angle should be reasonably consistent if the headings are accurate and the magnetic variation is correct. The angle should always be in the upstream direction if there is a moving bed. Using the Main.Details tab, the Avg Water Direction L/R Difference should be less than a few degrees if the compass is calibrated, the magnetic variation is correct, and there is no magnetic interference in the cross section.")
            self.compass["guidance"].append(
                self.guidance_prep(self.compass["messages"][-1][0], guidance_text)
            )
        elif meas.compass_cal[-1].result["compass"]["error"] == "N/A":
            # If the error cannot be decoded from the
            # calibration assume the calibration is good
            self.compass["status1"] = "good"
        else:
            if meas.compass_cal[-1].result["compass"]["error"] <= 0.2:
                self.compass["status1"] = "good"
            else:
                self.compass["status1"] = "caution"
                self.compass["messages"].append(
                    [self.tr("Compass: Calibration result > 0.2 deg") + ";", 2, 4]
                )
                guidance_text = self.tr("Experience has demonstrated that a calibration result greater than 0.2 degree could result in inconsistent headings. Accurate headings are critical when using GPS as a reference or using a loop moving-bed test. If in the field, try to recalibrate the compass (up to 3 times) near the measurement section but away from any magnetic interference. Your cell phone, keys, belt buckle are potential sources of interference if you are holding the ADCP. After 3 attempts that fail to be below 0.2 degree, document the results and proceed with the measurement. In the office, carefully evaluate the measurement. If GPS is used as the navigation reference, use the shiptrack plot and compare the angle between the GPS data and the BT data for reciprocal transects (turning off the vectors may help). The angle should be reasonably consistent if the headings are accurate and the magnetic variation is correct. The angle should always be in the upstream direction if there is a moving bed. Using the Main.Details tab, the Avg Water Direction L/R Difference should be less than a few degrees if the compass is calibrated, the magnetic variation is correct, and there is no magnetic interference in the cross section.")
                self.compass["guidance"].append(
                    self.guidance_prep(self.compass["messages"][-1][0], guidance_text)
                )

    def compass_qa_trdi_caleval(self, meas):
        """Evaluate compass calibration and/or evaluation for TRDI ADCPs.

         Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        # TRDI ADCP
        if len(meas.compass_cal) == 0:
            # No compass calibration
            if len(meas.compass_eval) == 0:
                # No calibration or evaluation
                self.compass["status1"] = "warning"
                self.compass["messages"].append(
                    [
                        self.tr("COMPASS: No compass calibration or evaluation") + ";",
                        1,
                        4,
                    ]
                )
                guidance_text = self.tr("Using GPS as the navigation reference and conducting a loop moving-bed test require an accurate compass. Substantial errors can occur if the compass is not accurate. If in the field, calibrated the compass and recollect the data. If in the office, carefully evaluate the measurement. If GPS is used as the navigation reference, use the shiptrack plot and compare the angle between the GPS data and the BT data for reciprocal transects (turning off the vectors may help). The angle should be reasonably consistent if the headings are accurate and the magnetic variation is correct. The angle should always be in the upstream direction if there is a moving bed. Using the Main.Details tab, the Avg Water Direction L/R Difference should be less than a few degrees if the compass is calibrated, the magnetic variation is correct, and there is no magnetic interference in the cross section.")
                self.compass["guidance"].append(
                    self.guidance_prep(self.compass["messages"][-1][0], guidance_text)
                )

            else:
                # No calibration but an evaluation was completed
                self.compass["status1"] = "caution"
                self.compass["messages"].append(
                    [self.tr("Compass: No compass calibration") + ";", 2, 4]
                )
                guidance_text = self.tr("If the evaluation result is < 1 degree a calibration is probably not necessary and the headings should be accurate. However, if the evaluation is greater than 1 degree a compass calibration should be completed. In the office, carefully evaluate the measurement. If GPS is used as the navigation reference, use the shiptrack plot and compare the angle between the GPS data and the BT data for reciprocal transects (turning off the vectors may help). The angle should be reasonably consistent if the headings are accurate and the magnetic variation is correct. The angle should always be in the upstream direction if there is a moving bed. Using the Main.Details tab, the Avg Water Direction L/R Difference should be less than a few degrees if the compass is calibrated, the magnetic variation is correct, and there is no magnetic interference in the cross section.")
                self.compass["guidance"].append(
                    self.guidance_prep(self.compass["messages"][-1][0], guidance_text)
                )
        else:
            # Compass was calibrated
            if len(meas.compass_eval) == 0:
                # No compass evaluation
                self.compass["status1"] = "caution"
                self.compass["messages"].append(
                    [self.tr("Compass: No compass evaluation") + ";", 2, 4]
                )
                guidance_text = self.tr("A compass evaluation provides information on the quality of the calibration. If in the field, complete an evaluation, even if it is after the measurement. In the office, carefully evaluate the measurement. If GPS is used as the navigation reference, use the shiptrack plot and compare the angle between the GPS data and the BT data for reciprocal transects (turning off the vectors may help). The angle should be reasonably consistent if the headings are accurate and the magnetic variation is correct. The angle should always be in the upstream direction if there is a moving bed. Using the Main.Details tab, the Avg Water Direction L/R Difference should be less than a few degrees if the compass is calibrated, the magnetic variation is correct, and there is no magnetic interference in the cross section.")
                self.compass["guidance"].append(
                    self.guidance_prep(self.compass["messages"][-1][0], guidance_text)
                )
            else:
                # Check results of evaluation
                try:
                    if float(meas.compass_eval[-1].result["compass"]["error"]) <= 1:
                        self.compass["status1"] = "good"
                    else:
                        self.compass["status1"] = "caution"
                        self.compass["messages"].append(
                            [self.tr("Compass: Evaluation result > 1 deg") + ";", 2, 4]
                        )
                        guidance_text = self.tr("If in the field, try to recalibrate the compass (up to 3 times) near the measurement section but away from any magnetic interference. Your cell phone, keys, belt buckle are potential sources of interference if you are holding the ADCP. After 3 attempts that fail to be below 1 degree, document the results and proceed with the measurement. In the office, carefully evaluate the measurement. If GPS is used as the navigation reference, use the shiptrack plot and compare the angle between the GPS data and the BT data for reciprocal transects (turning off the vectors may help). The angle should be reasonably consistent if the headings are accurate and the magnetic variation is correct. The angle should always be in the upstream direction if there is a moving bed. Using the Main.Details tab, the Avg Water Direction L/R Difference should be less than a few degrees if the compass is calibrated, the magnetic variation is correct, and there is no magnetic interference in the cross section.")
                        self.compass["guidance"].append(
                            self.guidance_prep(
                                self.compass["messages"][-1][0], guidance_text
                            )
                        )
                except ValueError:
                    self.compass["status1"] = "good"

    @staticmethod
    def compass_qa_hpr(meas):
        """Compute data and statistics needed to check the heading, pitch, and roll.

         Parameters
        ----------
        meas: Measurement
            Object of class Measurement

        Returns
        -------
        hpr: dict{}
            Dictionary with computed data and statistics.
        """

        # Initialize local variables
        magvar = []
        align = []
        mag_error_exceeded = []
        pitch_mean = []
        pitch_std = []
        pitch_exceeded = []
        roll_mean = []
        roll_std = []
        roll_exceeded = []
        left_water_dir = []
        right_water_dir = []

        # Compute transect properties to evaluation direction issues
        trans_prop = meas.compute_measurement_properties(meas)

        # Process each transect used for discharge
        for n, idx in enumerate(meas.checked_transect_idx):
            transect = meas.transects[idx]

            heading_source_selected = getattr(
                transect.sensors.heading_deg,
                transect.sensors.heading_deg.selected,
            )
            pitch_source_selected = getattr(
                transect.sensors.pitch_deg, transect.sensors.pitch_deg.selected
            )
            roll_source_selected = getattr(
                transect.sensors.roll_deg, transect.sensors.roll_deg.selected
            )

            magvar.append(transect.sensors.heading_deg.internal.mag_var_deg)
            if transect.sensors.heading_deg.external is not None:
                align.append(transect.sensors.heading_deg.external.align_correction_deg)

            pitch_mean.append(np.nanmean(pitch_source_selected.data))
            pitch_std.append(np.nanstd(pitch_source_selected.data, ddof=1))
            roll_mean.append(np.nanmean(roll_source_selected.data))
            roll_std.append(np.nanstd(roll_source_selected.data, ddof=1))

            if trans_prop["start_bank"][n] == "Left":
                left_water_dir.append(trans_prop["avg_water_dir"][n])
            else:
                right_water_dir.append(trans_prop["avg_water_dir"][n])

            # SonTek G3 compass provides pitch, roll, and magnetic
            # error parameters that can be checked
            if transect.adcp.manufacturer == "SonTek":
                if heading_source_selected.pitch_limit is not None:
                    # Check for bug in SonTek data where pitch and
                    # roll was n x 3 use n x 1
                    if len(pitch_source_selected.data.shape) == 1:
                        pitch_data = pitch_source_selected.data
                    else:
                        pitch_data = pitch_source_selected.data[:, 0]
                    idx_max = np.where(
                        pitch_data > heading_source_selected.pitch_limit[0]
                    )[0]
                    idx_min = np.where(
                        pitch_data < heading_source_selected.pitch_limit[1]
                    )[0]
                    if len(idx_max) > 0 or len(idx_min) > 0:
                        pitch_exceeded.append(True)
                    else:
                        pitch_exceeded.append(False)

                if heading_source_selected.roll_limit is not None:
                    if len(roll_source_selected.data.shape) == 1:
                        roll_data = roll_source_selected.data
                    else:
                        roll_data = roll_source_selected.data[:, 0]
                    idx_max = np.where(
                        roll_data > heading_source_selected.roll_limit[0]
                    )[0]
                    idx_min = np.where(
                        roll_data < heading_source_selected.roll_limit[1]
                    )[0]
                    if len(idx_max) > 0 or len(idx_min) > 0:
                        roll_exceeded.append(True)
                    else:
                        roll_exceeded.append(False)

                if heading_source_selected.mag_error is not None:
                    idx_max = np.where(heading_source_selected.mag_error > 2)[0]
                    if len(idx_max) > 0:
                        mag_error_exceeded.append(n)

        # LR difference water_direction
        diff_dir = np.nan
        if len(left_water_dir) > 0 and len(right_water_dir) > 0:
            diff_dir = np.abs(np.nanmean(left_water_dir) - np.nanmean(right_water_dir))
            if diff_dir > 180:
                diff_dir = diff_dir - 360

        hpr = {
            "magvar": magvar,
            "align": align,
            "mag_error_exceeded": mag_error_exceeded,
            "pitch_mean": pitch_mean,
            "pitch_std": pitch_std,
            "pitch_exceeded": pitch_exceeded,
            "roll_mean": roll_mean,
            "roll_std": roll_std,
            "roll_exceeded": roll_exceeded,
            "water_dir_diff": diff_dir,
        }

        return hpr

    def temperature_qa(self, meas):
        """Apply QA checks to temperature.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        self.temperature["messages"] = []
        self.temperature["guidance"] = []
        check = [0, 0]

        # Create array of all temperatures
        temp = np.array([])
        checked = []
        for transect in meas.transects:
            if transect.checked:
                checked.append(transect.checked)
                temp_selected = getattr(
                    transect.sensors.temperature_deg_c,
                    transect.sensors.temperature_deg_c.selected,
                )
                if len(temp) == 0:
                    temp = temp_selected.data
                else:
                    temp = np.hstack((temp, temp_selected.data))

        # Check temperature range
        if np.any(checked):
            temp_range = np.nanmax(temp) - np.nanmin(temp)
        else:
            temp_range = 0

        if temp_range > 2:
            check[0] = 3
            self.temperature["messages"].append(
                [
                    self.tr("TEMPERATURE: Temperature range is ")
                    + "{:3.1f}".format(temp_range)
                    + self.tr(" degrees C which is greater than 2 degrees") + ";",
                    1,
                    5,
                ]
            )
            guidance_text = self.tr("It is likely that the ADCP was not allowed to equilibrate to the water temperature prior to starting the measurement. However, it is also possible, though rare, that the water temperature is different from one side of the river to the other. If the temperature changes during the measurement and reaches an equilibrium value, then the ADCP was not given sufficient time to equilibrate. If the equilibrated ADCP temperature is close to the independent water temperature, change the water temperature source to user and enter either the independent temperature or the equilibrated ADCP temperature.")
            self.temperature["guidance"].append(
                self.guidance_prep(self.temperature["messages"][-1][0], guidance_text))
        elif temp_range > 1:
            check[0] = 2
            self.temperature["messages"].append(
                [
                    self.tr("Temperature: Temperature range is ")
                    + "{:3.1f}".format(temp_range)
                    + self.tr(" degrees C which is greater than 1 degree") + ";",
                    2,
                    5,
                ]
            )
            guidance_text = self.tr("It is likely that the ADCP was not allowed to equilibrate to the water temperature prior to starting the measurement. However, it is also possible, though rare, that the water temperature is different from one side of the river to the other. If the temperature changes during the measurement and reaches an equilibrium value, then the ADCP was not given sufficient time to equilibrate. If the equilibrated ADCP temperature is close to the independent water temperature, change the water temperature source to user and enter either the independent temperature or the equilibrated ADCP temperature.")
            self.temperature["guidance"].append(
                self.guidance_prep(self.temperature["messages"][-1][0], guidance_text))

        else:
            check[0] = 1

        # Check for independent temperature reading
        if "user" in meas.ext_temp_chk:
            try:
                user = float(meas.ext_temp_chk["user"])
            except (ValueError, TypeError):
                user = None
            if user is None or np.isnan(user):
                # No independent temperature reading
                check[1] = 2
                self.temperature["messages"].append(
                    [self.tr("Temperature: No independent temperature reading") + ";", 2, 5]
                )
                guidance_text = self.tr("The temperature measured by the ADCP cannot be verified without an independent temperature. The water temperature is critical to computing the speed of sound and the velocity from the Doppler shift. If still in the field, collect an independent water temperature. If in the office, look for other measurements using this ADCP and check that the ADCP temperature agrees with the water temperature in other measurements.")
                self.temperature["guidance"].append(
                    self.guidance_prep(self.temperature["messages"][-1][0],
                                       guidance_text))

            elif not np.isnan(meas.ext_temp_chk["adcp"]):
                # Compare user to manually entered ADCP temperature
                diff = np.abs(user - meas.ext_temp_chk["adcp"])
                if diff < 2:
                    check[1] = 1
                else:
                    check[1] = 3
                    self.temperature["messages"].append(
                        [
                            self.tr("TEMPERATURE: The difference between ADCP and reference is > 2:  ") + "{:3.1f}".format(diff) + " C;",
                            1,
                            5,
                        ]
                    )
                    guidance_text = self.tr("Ensure that the ADCP has had time to equilibrate to the water temperature and make another comparison. If the difference is still greater than 2 degrees, continue with the measurement. However, check the independent temperature source against another temperature source. If the independent temperature reading is correct, change the temperature source to user and enter the independent temperature source and reprocess the measurement.")
                    self.temperature["guidance"].append(
                        self.guidance_prep(self.temperature["messages"][-1][0],
                                           guidance_text))
            else:
                # Compare user to mean of all temperature data
                diff = np.abs(user - np.nanmean(temp))
                if diff < 2:
                    check[1] = 1
                else:
                    check[1] = 3
                    self.temperature["messages"].append(
                        [
                            self.tr("TEMPERATURE: The difference between ADCP and reference is > 2:  ") + "{:3.1f}".format(diff) + " C;",
                            1,
                            5,
                        ]
                    )
                    guidance_text = self.tr("Ensure that the ADCP has had time to equilibrate to the water temperature and make another comparison. If the difference is still greater than 2 degrees, continue with the measurement. However, check the independent temperature source against another temperature source. If the independent temperature reading is correct, change the temperature source to user and enter the independent temperature source and reprocess the measurement.")
                    self.temperature["guidance"].append(
                        self.guidance_prep(self.temperature["messages"][-1][0],
                                           guidance_text))

        # Assign temperature status
        max_check = max(check)
        if max_check == 1:
            self.temperature["status"] = "good"
        elif max_check == 2:
            self.temperature["status"] = "caution"
        elif max_check == 3:
            self.temperature["status"] = "warning"

    def moving_bed_qa(self, meas):
        """Applies quality checks to moving-bed tests.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        self.movingbed["messages"] = []
        self.movingbed["guidance"] = []
        self.movingbed["code"] = 0

        # Are there moving-bed tests?
        if len(meas.mb_tests) < 1:
            if meas.observed_no_moving_bed:
                self.movingbed["messages"].append(
                    [self.tr("Moving-Bed Test: Visually observed no moving bed") + ";", 2, 6]
                )
                guidance_text = self.tr("Provide documentation describing the visual observation and why it was determined that there was no moving bed.")
                self.movingbed["guidance"].append(
                    self.guidance_prep(self.movingbed["messages"][-1][0],
                                       guidance_text))
                self.movingbed["status"] = "caution"
                self.movingbed["code"] = 2
            else:
                # No moving-bed test
                self.movingbed["messages"].append(
                    [self.tr("MOVING-BED TEST: No moving bed test") + ";", 1, 6]
                )
                guidance_text = self.tr("A moving-bed test is required to determine if a moving-bed condition exists. If a moving-bed test cannot or was not collected, provide documentation and analysis as to the likely moving-bed condition at the time of the measurement.")
                self.movingbed["guidance"].append(
                    self.guidance_prep(self.movingbed["messages"][-1][0], guidance_text))
                self.movingbed["status"] = "warning"
                self.movingbed["code"] = 3

        else:
            # Moving-bed tests available
            mb_data = meas.mb_tests

            user_valid_test = []
            file_names = []
            idx_selected = []
            test_quality = []
            mb_tests = []
            mb = []
            mb_test_type = []
            loop = []
            use_2_correct = []
            gps_diff1 = False
            gps_diff2 = False

            for n, test in enumerate(mb_data):
                # Are tests valid according to the user
                if test.user_valid:
                    user_valid_test.append(True)
                    file_names.append(test.transect.file_name)
                    if test.type == "Loop" and not test.test_quality == "Errors":
                        loop.append(test.moving_bed)

                    if not np.isnan(test.gps_percent_mb):
                        if np.abs(test.bt_percent_mb - test.gps_percent_mb) > 2:
                            gps_diff2 = True
                        if np.logical_xor(
                            test.bt_percent_mb >= 1, test.gps_percent_mb >= 1
                        ):
                            gps_diff1 = True
                    # Selected test
                    if test.selected:
                        idx_selected.append(n)
                        test_quality.append(test.test_quality)
                        mb_tests.append(test)
                        mb.append(test.moving_bed)
                        mb_test_type.append(test.type)
                        use_2_correct.append(test.use_2_correct)
                else:
                    user_valid_test.append(False)

            if not any(user_valid_test):
                # No valid test according to user
                self.movingbed["messages"].append(
                    [
                        self.tr("MOVING-BED TEST: No valid moving-bed test based on user input") + ";",
                        1,
                        6,
                    ]
                )
                guidance_text = self.tr("Provide documentation why a valid moving-bed test could not be collected. Include in the documentation the likely moving-bed condition at the time of the measurement and  justification for that determination.")
                self.movingbed["guidance"].append(
                    self.guidance_prep(self.movingbed["messages"][-1][0], guidance_text))
                self.movingbed["status"] = "warning"
                self.movingbed["code"] = 3
            else:
                # Check for duplicate valid moving-bed tests
                if len(np.unique(file_names)) < len(file_names):
                    self.movingbed["messages"].append(
                        [
                            self.tr("MOVING-BED TEST: Duplicate moving-bed test files marked valid") + ";",
                            1,
                            6,
                        ]
                    )
                    guidance_text = self.tr("Duplicate filenames are typically a manufacturer's software bug. Only one of the duplicate tests should be used, that other should be marked invalid.")
                    self.movingbed["guidance"].append(
                        self.guidance_prep(self.movingbed["messages"][-1][0],
                                           guidance_text))
                    self.movingbed["status"] = "warning"
                    self.movingbed["code"] = 3

            if self.movingbed["code"] == 0:
                # Check test quality
                if len(test_quality) > 0 and sum(np.array(test_quality) == "Good") > 0:
                    self.movingbed["status"] = "good"
                    self.movingbed["code"] = 1

                    # Check if there is a moving-bed
                    if "Yes" in mb:
                        # Moving-bed present
                        self.movingbed["messages"].append(
                            [self.tr("Moving-Bed Test: A moving-bed is present") + ";", 2, 6]
                        )
                        guidance_text = self.tr("Moving-bed tests indicate a moving-bed condition. Use GPS for the navigation reference or use the moving-bed test results to correct the discharge for the effect of the moving-bed.")
                        self.movingbed["guidance"].append(
                            self.guidance_prep(self.movingbed["messages"][-1][0],
                                               guidance_text))
                        self.movingbed["code"] = 2
                        self.movingbed["status"] = "caution"
                        if (
                            meas.transects[
                                meas.checked_transect_idx[0]
                            ].boat_vel.composite
                            == "On"
                        ):
                            self.movingbed["messages"].append(
                                [
                                    self.tr("Moving-Bed: Use of composite tracks could cause inaccurate results") + ";",
                                    2,
                                    6,
                                ]
                            )
                            guidance_text = self.tr("Use of composite tracks in moving-bed conditions is generally not recommended. GPS data are not affected by moving-bed conditions but bottom track data are biased by moving-bed conditions. So mixing of bottom track and GPS data will result in inconsistent handling of moving-bed effects.")
                            self.movingbed["guidance"].append(
                                self.guidance_prep(self.movingbed["messages"][-1][0],
                                                   guidance_text))

                        if (
                            meas.transects[
                                meas.checked_transect_idx[0]
                            ].boat_vel.selected
                            == "bt_vel"
                        ):
                            if any(use_2_correct):
                                self.movingbed["messages"].append(
                                    [
                                        self.tr("Moving-Bed: BT based moving-bed correction applied") + ";",
                                        2,
                                        6,
                                    ]
                                )
                                guidance_text = self.tr("A moving-bed is present and the moving-bed test has been applied to correct the discharge because bottom track is used as the reference. If valid GPS data are available, it is recommended to use GPS as the navigation reference.")
                                self.movingbed["guidance"].append(
                                    self.guidance_prep(self.movingbed["messages"][-1][0],
                                                       guidance_text))
                            else:
                                self.movingbed["messages"].append(
                                    [
                                        self.tr("MOVING-BED: Moving-bed present and BT used, but no correction applied") + ";",
                                        1,
                                        6,
                                    ]
                                )
                                guidance_text = self.tr("A moving-bed is present, but the user has manually turned off the correction. Lack of correction in a moving-bed condition will result in a discharge that is biased low. The discharge should be corrected by a moving-bed test result or GPS should be used for the navigation reference.")
                                self.movingbed["guidance"].append(
                                    self.guidance_prep(self.movingbed["messages"][-1][0],
                                                       guidance_text))
                                self.movingbed["code"] = 3
                                self.movingbed["status"] = "warning"
                        elif (
                            meas.transects[
                                meas.checked_transect_idx[0]
                            ].boat_vel.selected
                            == "gga_vel"
                        ):
                            self.movingbed["messages"].append(
                                [self.tr("Moving-Bed: GGA used") + ";", 2, 6]
                            )
                            guidance_text = self.tr("A moving-bed is present. GGA is used. Verify the GGA data are valid.")
                            self.movingbed["guidance"].append(
                                self.guidance_prep(self.movingbed["messages"][-1][0],
                                                   guidance_text))
                        elif (
                            meas.transects[
                                meas.checked_transect_idx[0]
                            ].boat_vel.selected
                            == "vtg_vel"
                        ):
                            self.movingbed["messages"].append(
                                [self.tr("Moving-Bed: VTG used") + ";", 2, 6]
                            )
                            guidance_text = self.tr("A moving-bed is present. VTG is used. Verify the VTG data are valid.")
                            self.movingbed["guidance"].append(
                                self.guidance_prep(self.movingbed["messages"][-1][0],
                                                   guidance_text))

                        # Check for test type
                        if sum(np.array(mb_test_type) == "Stationary"):
                            # Check for GPS or 3 stationary tests
                            if len(mb_tests) < 3:
                                gps = []
                                for transect in meas.transects:
                                    if transect.checked:
                                        if transect.gps is None:
                                            gps.append(False)
                                        else:
                                            gps.append(True)
                                if not all(gps):
                                    # GPS not available for all selected
                                    # transects
                                    self.movingbed["messages"].append(
                                        [
                                            self.tr("Moving-Bed Test: Less than 3 stationary tests available for moving-bed correction") + ";",
                                            2,
                                            6,
                                        ]
                                    )
                                    guidance_text = self.tr("A moving-bed is present and the results of the stationary moving-bed test will be used to correct the discharge for the effects of the moving-bed. Moving-bed conditions vary across the channel, so at least 3 stationary tests distributed evenly across the channel are recommended to capture the variability of the moving-bed and provide a better correction for the discharge. A correction will be computed using only one test, but a better result would be obtained with at least 3 stationary tests.")
                                    self.movingbed["guidance"].append(self.guidance_prep(
                                        self.movingbed["messages"][-1][0], guidance_text))

                elif (
                    len(test_quality) > 0
                    and sum(np.array(test_quality) == "Warnings") > 0
                ):
                    # Quality check has warnings
                    self.movingbed["messages"].append(
                        [
                            self.tr("Moving-Bed Test: The moving-bed test(s) has warnings, please review tests to determine validity") + ";",
                            2,
                            6,
                        ]
                    )
                    guidance_text = self.tr("Review the tests to determine if they are valid. If in the field, consider collecting another moving-bed test, perhaps using a different method (stationary or loop).")
                    self.movingbed["guidance"].append(
                        self.guidance_prep(self.movingbed["messages"][-1][0],
                                           guidance_text))

                    self.movingbed["status"] = "caution"
                    self.movingbed["code"] = 2

                elif (
                    len(test_quality) > 0
                    and sum(np.array(test_quality) == "Manual") > 0
                ):
                    # Manual override used
                    self.movingbed["messages"].append(
                        [
                            self.tr("MOVING-BED TEST: The user has manually forced the use of some tests") + ";",
                            1,
                            6,
                        ]
                    )
                    guidance_text = self.tr("Justification for using moving-bed tests with critical errors should be provided in the documentation.")
                    self.movingbed["guidance"].append(
                        self.guidance_prep(self.movingbed["messages"][-1][0],
                                           guidance_text))
                    self.movingbed["status"] = "warning"
                    self.movingbed["code"] = 3

                else:
                    # Test has critical errors
                    self.movingbed["messages"].append(
                        [self.tr( "MOVING-BED TEST: The moving-bed test(s) have critical errors and will not be used") + ";",
                            1,
                            6,
                        ]
                    )
                    guidance_text = self.tr("If in the field, consider collecting another moving-bed test, perhaps using a different method (stationary or loop). If a moving-bed test cannot or was not collected provide documentation and analysis as to the likely moving-bed condition at the time of the measurement.")
                    self.movingbed["guidance"].append(
                        self.guidance_prep(self.movingbed["messages"][-1][0],
                                           guidance_text))

                    self.movingbed["status"] = "warning"
                    self.movingbed["code"] = 3

                # Check multiple loops for consistency
                if len(np.unique(loop)) > 1:
                    self.movingbed["messages"].append(
                        [
                            self.tr("Moving-Bed Test: Results of valid loops are not consistent, review moving-bed tests") + ";",
                            2,
                            6,
                        ]
                    )
                    guidance_text = self.tr("The loop tests are not consistent. Review the tests and determine which test should be used by marking the other tests invalid. By default QRev will use the last valid loop test.")
                    self.movingbed["guidance"].append(
                        self.guidance_prep(self.movingbed["messages"][-1][0],
                                           guidance_text))
                    if self.movingbed["code"] < 3:
                        self.movingbed["code"] = 2
                        self.movingbed["status"] = "caution"

                # Notify of differences in results of test between BT and GPS
                if gps_diff2:
                    self.movingbed["messages"].append(
                        [
                            self.tr("Moving-Bed Test: Bottom track and GPS results differ by more than 2%") + ";",
                            2,
                            6,
                        ]
                    )
                    guidance_text = self.tr("GPS data are available for the moving-bed tests. The test results are computed assuming a loop test returned to the same starting position and a the ADCP was stationary during the stationary test. Using GPS to identify the start and stop points for a loop test and to track the movement of the ADCP during the stationary test provides a check on those base assumptions. If there is a difference the user should verify that the GPS data appear valid and then assess whether the loop test returned to the same starting location or if the stationary test may be bias by movement of the ADCP. Selecting which to use is based on the user knowledge of the test and the validation of the data.")
                    self.movingbed["guidance"].append(
                        self.guidance_prep(self.movingbed["messages"][-1][0],
                                           guidance_text))
                    if self.movingbed["code"] < 3:
                        self.movingbed["code"] = 2
                        self.movingbed["status"] = "caution"

                if gps_diff1:
                    self.movingbed["messages"].append(
                        [
                            self.tr("Moving-Bed Test: Bottom track and GPS results do not agree") + ";",
                            2,
                            6,
                        ]
                    )
                    guidance_text = self.tr("GPS data are available for the moving-bed tests. The test results are computed assuming a loop test returned to the same starting position and the ADCP was stationary during a stationary test. Using GPS to identify the start and stop points for a loop test and to track the movement of the ADCP during the stationary test provides a check on those base assumptions. If there is a difference, the user should verify that the GPS data appear valid and then assess whether the loop test returned to the same starting location or if the stationary test may be bias by movement of the ADCP. Selecting which to use is based on the user's knowledge of the test and the validation of the data.")
                    self.movingbed["guidance"].append(
                        self.guidance_prep(self.movingbed["messages"][-1][0],
                                           guidance_text))
                    if self.movingbed["code"] < 3:
                        self.movingbed["code"] = 2
                        self.movingbed["status"] = "caution"

            if len(loop) > 0:
                if "Loop" in mb_test_type:
                    if self.compass["status"] == "inactive":
                        self.movingbed["messages"].append(
                            self.tr("MOVING-BED TEST: Loop test is not valid. ADCP has no compass") + ";"
                        )
                        guidance_text = self.tr("A loop test requires a accurate compass. Collect a stationary test instead of a loop test.")
                        self.movingbed["guidance"].append(
                            self.guidance_prep(self.movingbed["messages"][-1][0],
                                               guidance_text))
                        self.movingbed["code"] = 3
                        self.movingbed["status"] = "caution"

                    if self.compass["status1"] != "good":
                        self.movingbed["messages"].append(
                            [
                                self.tr("Moving-Bed Test: Loop test used but compass calibration is ")
                                + self.compass["status1"],
                                2,
                                6,
                            ]
                        )
                        guidance_text = self.tr("A loop test requires a accurate compass. Recalibrate the compass prior to collecting another loop test or collect a stationary test instead of a loop test.")
                        self.movingbed["guidance"].append(
                            self.guidance_prep(self.movingbed["messages"][-1][0],
                                               guidance_text))
                        if self.movingbed["code"] < 3:
                            self.movingbed["code"] = 2
                            self.movingbed["status"] = "caution"

        self.check_mbt_settings(meas)

    def user_qa(self, meas):
        """Apply quality checks to user input data.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        self.user["messages"] = []
        self.user["guidance"] = []
        self.user["status"] = "good"

        # Check for Station Name
        self.user["sta_name"] = False
        if meas.station_name is None or len(meas.station_name.strip()) < 1:
            self.user["messages"].append([self.tr("Site Info: Station name not entered") + ";", 2, 2])
            guidance_text = self.tr("Enter site name or description of site location to identify the location of this measurement.")
            self.user["guidance"].append(
                self.guidance_prep(self.user["messages"][-1][0], guidance_text))
            self.user["status"] = "caution"
            self.user["sta_name"] = True

        # Check for Station Number
        self.user["sta_number"] = False
        try:
            if meas.station_number is None or len(meas.station_number.strip()) < 1:
                self.user["messages"].append(
                    [self.tr("Site Info: Station number not entered") + ";", 2, 2]
                )
                guidance_text = self.tr("If the measurement was made at numbered gauging site, enter the site number.")
                self.user["guidance"].append(
                    self.guidance_prep(self.user["messages"][-1][0], guidance_text))
                self.user["status"] = "caution"
                self.user["sta_number"] = True
        except AttributeError:
            self.user["messages"].append(
                [self.tr("Site Info: Station number not entered") + ";", 2, 2]
            )
            guidance_text = self.tr(
                "If the measurement was made at numbered gauging site, enter the site number.")
            self.user["guidance"].append(
                self.guidance_prep(self.user["messages"][-1][0], guidance_text))
            self.user["status"] = "caution"
            self.user["sta_number"] = True

        # Time zone
        self.user["time_zone"] = False
        if meas.time_zone_required:
            if len(meas.time_zone) == 0:
                self.user["messages"].append(
                    [
                        self.tr("Time Zone: Your agency requires the time zone to be entered") + ";",
                        2,
                        2,
                    ]
                )
                guidance_text = self.tr("Your agency requires the time zone to be entered. Select the appropriate time zone from the list on the Premeasurement tab.")
                self.user["guidance"].append(
                    self.guidance_prep(self.user["messages"][-1][0], guidance_text))
                self.user["status"] = "caution"
                self.user["time_zone"] = True

    def depths_qa(self, meas):
        """Apply quality checks to depth data.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        # Initialize variables
        n_transects = len(meas.transects)
        self.depths["q_total"] = np.tile(np.nan, n_transects)
        self.depths["q_max_run"] = np.tile(np.nan, n_transects)
        self.depths["q_total_caution"] = np.tile(False, n_transects)
        self.depths["q_max_run_caution"] = np.tile(False, n_transects)
        self.depths["q_total_warning"] = np.tile(False, n_transects)
        self.depths["q_max_run_warning"] = np.tile(False, n_transects)
        self.depths["all_invalid"] = np.tile(False, n_transects)
        self.depths["messages"] = []
        self.depths["guidance"] = []
        self.depths["status"] = "good"
        self.depths["draft"] = 0
        checked = []
        drafts = []
        for n, transect in enumerate(meas.transects):
            checked.append(transect.checked)
            if transect.checked:
                in_transect_idx = transect.in_transect_idx

                depths_selected = getattr(transect.depths, transect.depths.selected)
                drafts.append(depths_selected.draft_use_m)

                # Determine valid measured depths
                if transect.depths.composite:
                    depth_na = depths_selected.depth_source_ens[in_transect_idx] != "NA"
                    depth_in = depths_selected.depth_source_ens[in_transect_idx] != "IN"
                    depth_valid = np.all(np.vstack((depth_na, depth_in)), 0)
                else:
                    depth_valid_temp = depths_selected.valid_data[in_transect_idx]
                    depth_nan = (
                        depths_selected.depth_processed_m[in_transect_idx] != np.nan
                    )
                    depth_valid = np.all(np.vstack((depth_nan, depth_valid_temp)), 0)

                if not np.any(depth_valid):
                    self.depths["all_invalid"][n] = True

                # Compute QA characteristics
                q_total, q_max_run, number_invalid_ensembles = QAData.invalid_qa(
                    depth_valid, meas.discharge[n]
                )
                self.depths["q_total"][n] = q_total
                self.depths["q_max_run"][n] = q_max_run

                # Compute percentage compared to total
                if meas.discharge[n].total == 0.0:
                    q_total_percent = np.nan
                    q_max_run_percent = np.nan
                else:
                    q_total_percent = np.abs((q_total / meas.discharge[n].total) * 100)
                    q_max_run_percent = np.abs(
                        (q_max_run / meas.discharge[n].total) * 100
                    )

                # Apply total interpolated discharge threshold
                if q_total_percent > self.q_total_threshold_warning:
                    self.depths["q_total_warning"][n] = True
                elif q_total_percent > self.q_total_threshold_caution:
                    self.depths["q_total_caution"][n] = True

                # Apply interpolated discharge run thresholds
                if q_max_run_percent > self.q_run_threshold_warning:
                    self.depths["q_max_run_warning"][n] = True
                elif q_max_run_percent > self.q_run_threshold_caution:
                    self.depths["q_max_run_caution"][n] = True

        if checked:
            # Create array of all unique draft values
            draft_check = np.unique(np.round(drafts, 2))

            # Check draft consistency
            if len(draft_check) > 1:
                self.depths["status"] = "caution"
                self.depths["draft"] = 1
                self.depths["messages"].append(
                    [
                        self.tr("Depth: Transducer depth is not consistent among transects") + ";",
                        2,
                        10,
                    ]
                )
                guidance_text = self.tr("Generally the depth of the transducer is set at the beginning of the measurement and not changed. If the change in the depth of the transducer is correct, provide documentation, if not, set the depth of transducer to the correct value.")
                self.depths["guidance"].append(
                    self.guidance_prep(self.depths["messages"][-1][0], guidance_text))

            # Check for zero draft
            if np.any(np.less(draft_check, 0.01)):
                self.depths["status"] = "warning"
                self.depths["draft"] = 2
                self.depths["messages"].append(
                    [self.tr("DEPTH: Transducer depth is too shallow, likely 0") + ";", 1, 10]
                )
                guidance_text = self.tr("The transducer must be submerged, thus a value less than 0.01 m is not reasonable. Set the correct depth of transducer.")
                self.depths["guidance"].append(
                    self.guidance_prep(self.depths["messages"][-1][0], guidance_text))

            # Check interpolated discharge criteria
            if np.any(self.depths["q_max_run_warning"]):
                self.depths["messages"].append(
                    [
                        self.tr("DEPTH: Int. Q for consecutive invalid ensembles exceeds ")
                        + "%2.0f" % self.q_run_threshold_warning
                        + "%;",
                        1,
                        10,
                    ]
                )
                guidance_text = self.tr("More than 5% of the discharge was computed for consecutive ensembles with invalid depths using interpolated depths. Check that the shape of the cross section is reasonable using the interpolated depths.")
                self.depths["guidance"].append(
                    self.guidance_prep(self.depths["messages"][-1][0], guidance_text))
                self.depths["status"] = "warning"

            elif np.any(self.depths["q_total_warning"]):
                self.depths["messages"].append(
                    [
                        self.tr("DEPTH: Int. Q for invalid ensembles in a transect exceeds ")
                        + "%2.0f" % self.q_total_threshold_warning
                        + "%;",
                        1,
                        10,
                    ]
                )
                guidance_text = self.tr("More than 25% of the discharge was computed for ensembles with invalid depths using interpolated depths. Check that the shape of the cross section is reasonable using the interpolated depths.")
                self.depths["guidance"].append(
                    self.guidance_prep(self.depths["messages"][-1][0], guidance_text))
                self.depths["status"] = "warning"
            elif np.any(self.depths["q_max_run_caution"]):
                self.depths["messages"].append(
                    [
                        self.tr("Depth: Int. Q for consecutive invalid ensembles exceeds ")
                        + "%2.0f" % self.q_run_threshold_caution
                        + "%;",
                        2,
                        10,
                    ]
                )
                guidance_text = self.tr("More than 3% of the discharge was computed for consecutive ensembles with invalid depths using interpolated depths. Check that the shape of the cross section is reasonable using the interpolated depths.")
                self.depths["guidance"].append(
                    self.guidance_prep(self.depths["messages"][-1][0], guidance_text))
                self.depths["status"] = "caution"
            elif np.any(self.depths["q_total_caution"]):
                self.depths["messages"].append(
                    [
                        self.tr("Depth: Int. Q for invalid ensembles in a transect exceeds ")
                        + "%2.0f" % self.q_total_threshold_caution
                        + "%;",
                        2,
                        10,
                    ]
                )
                guidance_text = self.tr("More than 10% of the discharge was computed for consecutive ensembles with invalid depths using interpolated depths. Check that the shape of the cross section is reasonable using the interpolated depths.")
                self.depths["guidance"].append(
                    self.guidance_prep(self.depths["messages"][-1][0], guidance_text))
                self.depths["status"] = "caution"

            # Check if all depths are invalid
            if np.any(self.depths["all_invalid"]):
                self.depths["messages"].append(
                    [
                        self.tr("DEPTH: There are no valid depths for one or more transects") + ";",
                        2,
                        10,
                    ]
                )
                guidance_text = self.tr("Review the depth filters and measured depths to ensure that the filter settings are appropriate. If there are no valid depths, remove the transect from consideration in computing discharge.")
                self.depths["guidance"].append(
                    self.guidance_prep(self.depths["messages"][-1][0], guidance_text))
                self.depths["status"] = "warning"

        else:
            self.depths["status"] = "inactive"

    def boat_qa(self, meas):
        """Apply quality checks to boat data.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        # Initialize variables
        n_transects = len(meas.transects)
        data_type = {
            "BT": {
                "class": "bt_vel",
                "warning": "BT-",
                "caution": "bt-",
                "filter": [
                    (self.tr("All") + ": ", 0),
                    (self.tr("Original") + ": ", 1),
                    (self.tr("ErrorVel") + ": ", 2),
                    (self.tr("VertVel") + ": ", 3),
                    (self.tr("Other") + ": ", 4),
                    (self.tr("3Beams") + ": ", 5),
                ],
            },
            "GGA": {
                "class": "gga_vel",
                "warning": "GGA-",
                "caution": "gga-",
                "filter": [
                    (self.tr("All") + ": ", 0),
                    (self.tr("Original") + ": ", 1),
                    (self.tr("DGPS") + ": ", 2),
                    (self.tr("Altitude") + ": ", 3),
                    (self.tr("Other") + ": ", 4),
                    (self.tr("HDOP") + ": ", 5),
                ],
            },
            "VTG": {
                "class": "vtg_vel",
                "warning": "VTG-",
                "caution": "vtg-",
                "filter": [
                    (self.tr("All") + ": ", 0),
                    (self.tr("Original") + ": ", 1),
                    (self.tr("Other") + ": ", 4),
                    (self.tr("HDOP") + ": ", 5),
                ],
            },
        }
        self.boat["messages"] = []
        self.boat["guidance"] = []

        for dt_key, dt_value in data_type.items():
            boat = getattr(self, dt_value["class"])

            # Initialize dictionaries for each data type
            boat["q_total_caution"] = np.tile(False, (n_transects, 6))
            boat["q_max_run_caution"] = np.tile(False, (n_transects, 6))
            boat["q_total_warning"] = np.tile(False, (n_transects, 6))
            boat["q_max_run_warning"] = np.tile(False, (n_transects, 6))
            boat["all_invalid"] = np.tile(False, n_transects)
            boat["q_total"] = np.tile(np.nan, (n_transects, 6))
            boat["q_max_run"] = np.tile(np.nan, (n_transects, 6))
            boat["messages"] = []
            boat["guidance"] = []
            status_switch = 0
            avg_speed_check = 0

            # Check the results of each filter
            for dt_filter in dt_value["filter"]:
                boat["status"] = "inactive"

                # Quality check each transect
                for n, transect in enumerate(meas.transects):
                    # Evaluate on transects used in the discharge computation
                    if transect.checked:
                        in_transect_idx = transect.in_transect_idx

                        # Check to see if data are available for the data_type
                        if getattr(transect.boat_vel, dt_value["class"]) is not None:
                            boat["status"] = "good"

                            # Compute quality characteristics
                            valid = getattr(
                                transect.boat_vel, dt_value["class"]
                            ).valid_data[dt_filter[1], in_transect_idx]

                            # Check if all invalid
                            if dt_filter[1] == 0 and not np.any(valid):
                                boat["all_invalid"][n] = True

                            else:
                                (
                                    q_total,
                                    q_max_run,
                                    number_invalid_ens,
                                ) = QAData.invalid_qa(valid, meas.discharge[n])
                                boat["q_total"][n, dt_filter[1]] = q_total
                                boat["q_max_run"][n, dt_filter[1]] = q_max_run

                                # Compute percentage compared to total
                                if meas.discharge[n].total == 0.0:
                                    q_total_percent = np.nan
                                    q_max_run_percent = np.nan
                                else:
                                    q_total_percent = np.abs(
                                        (q_total / meas.discharge[n].total) * 100
                                    )
                                    q_max_run_percent = np.abs(
                                        (q_max_run / meas.discharge[n].total) * 100
                                    )

                                # Apply total interpolated discharge threshold
                                if q_total_percent > self.q_total_threshold_warning:
                                    boat["q_total_warning"][n, dt_filter[1]] = True
                                elif q_total_percent > self.q_total_threshold_caution:
                                    boat["q_total_caution"][n, dt_filter[1]] = True

                                # Apply interpolated discharge run thresholds
                                if q_max_run_percent > self.q_run_threshold_warning:
                                    boat["q_max_run_warning"][n, dt_filter[1]] = True
                                elif q_max_run_percent > self.q_run_threshold_caution:
                                    boat["q_max_run_caution"][n, dt_filter[1]] = True

                                # Check boat velocity for vtg data
                                if (
                                    dt_key == "VTG"
                                    and transect.boat_vel.selected == "vtg_vel"
                                    and avg_speed_check == 0
                                ):
                                    if transect.boat_vel.vtg_vel.u_mps is not None:
                                        avg_speed = np.nanmean(
                                            (
                                                transect.boat_vel.vtg_vel.u_mps**2
                                                + transect.boat_vel.vtg_vel.v_mps**2
                                            )
                                            ** 0.5
                                        )
                                        if avg_speed < 0.24:
                                            # boat["q_total_caution"][n, 2] = True
                                            if status_switch < 1:
                                                status_switch = 1
                                            boat["messages"].append(
                                                [
                                                    self.tr("vtg-AvgSpeed: VTG data may not be accurate for average boat speed less than 0.24 m/s (0.8  ft/s)") + ";",
                                                    2,
                                                    8,
                                                ]
                                            )
                                            guidance_text = self.tr("VTG velocities are based on a Doppler shift in the satellite signals. At velocities lower than 0.24 m/s these velocities may not be accurate in either magnitude and/or direction.")
                                            boat["guidance"].append(
                                                self.guidance_prep(
                                                    boat["messages"][-1][0],
                                                    guidance_text))
                                            avg_speed_check = 1

            # Create message for consecutive invalid discharge
            if boat["q_max_run_warning"].any():
                if dt_key == "BT":
                    module_code = 7
                else:
                    module_code = 8
                boat["messages"].append(
                    [
                        dt_value["warning"]
                        + self.tr("Int. Q for consecutive invalid ensembles exceeds ")
                        + "%3.1f" % self.q_run_threshold_warning
                        + "%;",
                        1,
                        module_code,
                    ]
                )
                guidance_text = self.tr("More than 5% of the discharge base on the reference is invalid in a consecutive group. Carefully review the time series and shiptrack to ensure that the linear interpolation of invalid data appear reasonable.")
                boat["guidance"].append(
                    self.guidance_prep(boat["messages"][-1][0], guidance_text))
                status_switch = 2
            elif boat["q_total_warning"].any():
                if dt_key == "BT":
                    module_code = 7
                else:
                    module_code = 8
                boat["messages"].append(
                    [
                        dt_value["warning"]
                        + self.tr("Int. Q for invalid ensembles in a transect exceeds ") 
                        + "%3.1f" % self.q_total_threshold_warning + "%;",
                        1,
                        module_code,
                    ]
                )
                guidance_text = self.tr("More than 25% of the discharge base on the reference is invalid. Carefully review the time series and shiptrack to ensure that the linear interpolation of invalid data appear reasonable.")
                boat["guidance"].append(
                    self.guidance_prep(boat["messages"][-1][0], guidance_text))
                status_switch = 2
            elif boat["q_max_run_caution"].any():
                if dt_key == "BT":
                    module_code = 7
                else:
                    module_code = 8
                boat["messages"].append(
                    [
                        dt_value["caution"]
                        + self.tr("Int. Q for consecutive invalid ensembles exceeds ")
                        + "%3.1f" % self.q_run_threshold_caution
                        + "%;",
                        2,
                        module_code,
                    ]
                )
                guidance_text = self.tr("More than 3% of the discharge base on the reference is invalid in a consecutive group. Carefully review the time series and shiptrack to ensure that the linear interpolation of invalid data appear reasonable.")
                boat["guidance"].append(
                    self.guidance_prep(boat["messages"][-1][0], guidance_text))
                if status_switch < 1:
                    status_switch = 1

            elif boat["q_total_caution"].any():
                if dt_key == "BT":
                    module_code = 7
                else:
                    module_code = 8
                boat["messages"].append(
                    [
                        dt_value["caution"]
                        + self.tr("Int. Q for invalid ensembles in a transect exceeds ")
                        + "%3.1f" % self.q_total_threshold_caution + "%;",
                        2,
                        module_code,
                    ]
                )
                guidance_text = self.tr("More than 10% of the discharge base on the reference is invalid. Carefully review the time series and shiptrack to ensure that the linear interpolation of invalid data appear reasonable.")
                boat["guidance"].append(
                    self.guidance_prep(boat["messages"][-1][0], guidance_text))
                if status_switch < 1:
                    status_switch = 1

            # Create message for all data invalid
            if boat["all_invalid"].any():
                boat["status"] = "warning"
                if dt_key == "BT":
                    module_code = 7
                else:
                    module_code = 8
                boat["messages"].append(
                    [
                        dt_value["warning"]
                        + dt_value["filter"][0][0]
                        + self.tr("There are no valid data for one or more transects") + ";",
                        1,
                        module_code,
                    ]
                )
                guidance_text = self.tr("Carefully review the data with special attention to the identified filter. If for some reason the filter results do not appear reasonable, change the filter setting. Provide justification for any changes made.")
                boat["guidance"].append(
                    self.guidance_prep(boat["messages"][-1][0], guidance_text))

            # Set status
            if status_switch == 2:
                boat["status"] = "warning"
            elif status_switch == 1:
                boat["status"] = "caution"

            setattr(self, dt_value["class"], boat)

        lag_gga = []
        lag_vtg = []
        self.gga_vel["lag_status"] = "good"
        self.vtg_vel["lag_status"] = "good"
        for transect in meas.transects:
            gga, vtg = TransectData.compute_gps_lag(transect)
            if gga is not None:
                lag_gga.append(gga)
            if vtg is not None:
                lag_vtg.append(vtg)
        if len(lag_gga) > 0:
            if np.mean(np.abs(lag_gga)) > 10:
                self.gga_vel["messages"].append(
                    [self.tr("GGA: BT and GGA do not appear to be sychronized") + ";", 1, 8]
                )
                guidance_text = self.tr("The GGA data are not synchronized with the ADCP data, which if GGA is used as the reference will result in incorrect water velocities. This could be due to serial port buffering or filters used by the GPS receiver. Turn off filters on the GPS receiver. Try reducing the update rate from the GPS receiver to 2 Hz. If the baud rate is > 19.2k, try reducing to 19.2k. If the baud rate is lower than 115.2k try increasing the baud rate.")
                self.gga_vel["guidance"].append(
                    self.guidance_prep(self.gga_vel["messages"][-1][0], guidance_text))
                if self.gga_vel["status"] != "warning":
                    self.gga_vel["status"] = "warning"
                    self.gga_vel["lag_status"] = "warning"
            elif np.mean(np.abs(lag_gga)) > 2:
                self.gga_vel["messages"].append(
                    [self.tr("gga: Lag between BT and GGA > 2 sec") + ";", 2, 8]
                )
                guidance_text = self.tr("The GGA data are not synchronized with the ADCP data, which if GGA is used as the reference will result in incorrect water velocities. This could be due to serial port buffering or filters used by the GPS receiver. Turn off filters on the GPS receiver. Try reducing the update rate from the GPS receiver to 2 Hz. If the baud rate is > 19.2k, try reducing to 19.2k. If the baud rate is lower than 115.2k try increasing the baud rate.")
                self.gga_vel["guidance"].append(
                    self.guidance_prep(self.gga_vel["messages"][-1][0], guidance_text))
                if self.gga_vel["status"] != "warning":
                    self.gga_vel["status"] = "caution"
                    self.gga_vel["lag_status"] = "caution"
        if len(lag_vtg) > 0:
            if np.mean(np.abs(lag_vtg)) > 10:
                self.vtg_vel["messages"].append(
                    [self.tr("VTG: BT and VTG do not appear to be sychronized") + ";", 1, 8]
                )
                guidance_text = self.tr("The VTG data are not synchronized with the ADCP data, which if GGA is used as the reference will result in incorrect water velocities. This could be due to serial port buffering or filters used by the GPS receiver. Turn off filters on the GPS receiver. Try reducing the update rate from the GPS receiver to 2 Hz. If the baud rate is > 19.2k, try reducing to 19.2k. If the baud rate is lower than 115.2k try increasing the baud rate.")
                self.vtg_vel["guidance"].append(
                    self.guidance_prep(self.vtg_vel["messages"][-1][0], guidance_text))
                if self.vtg_vel["status"] != "warning":
                    self.vtg_vel["status"] = "warning"
                    self.vtg_vel["lag status"] = "warning"
            elif np.mean(np.abs(lag_vtg)) > 2:
                self.vtg_vel["messages"].append(
                    [self.tr("vtg: Lag between BT and VTG > 2 sec") + ";", 2, 8]
                )
                guidance_text = self.tr("The VTG data are not synchronized with the ADCP data, which if GGA is used as the reference will result in incorrect water velocities. This could be due to serial port buffering or filters used by the GPS receiver. Turn off filters on the GPS receiver. Try reducing the update rate from the GPS receiver to 2 Hz. If the baud rate is > 19.2k, try reducing to 19.2k. If the baud rate is lower than 115.2k try increasing the baud rate.")
                self.vtg_vel["guidance"].append(
                    self.guidance_prep(self.vtg_vel["messages"][-1][0], guidance_text))
                if self.vtg_vel["status"] != "warning":
                    self.vtg_vel["status"] = "caution"
                    self.vtg_vel["lag_status"] = "caution"

    def water_qa(self, meas):
        """Apply quality checks to water data.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        # Initialize filter labels and indices
        if meas.transects[0].adcp.manufacturer == "TRDI":
            filter_index = [0, 1, 2, 3, 4, 5]
        else:
            filter_index = [0, 1, 2, 3, 4, 5, 7]

        n_transects = len(meas.transects)
        n_filters = len(filter_index) + 1
        # Initialize dictionaries for each data type
        self.w_vel["q_total_caution"] = np.tile(False, (n_transects, n_filters))
        self.w_vel["q_max_run_caution"] = np.tile(False, (n_transects, n_filters))
        self.w_vel["q_total_warning"] = np.tile(False, (n_transects, n_filters))
        self.w_vel["q_max_run_warning"] = np.tile(False, (n_transects, n_filters))
        self.w_vel["all_invalid"] = np.tile(False, n_transects)
        self.w_vel["q_total"] = np.tile(np.nan, (n_transects, n_filters))
        self.w_vel["q_max_run"] = np.tile(np.nan, (n_transects, n_filters))
        self.w_vel["messages"] = []
        self.w_vel["guidance"] = []
        status_switch = 0

        # TODO if meas had a property checked as list it would save creating
        #  that list multiple times
        checked = []
        for transect in meas.transects:
            checked.append(transect.checked)

        # At least one transect is being used to compute discharge
        if any(checked):
            # Loop through filters
            for prefix_idx, filter_idx in enumerate(filter_index):
                # Loop through transects
                for n, transect in enumerate(meas.transects):
                    if transect.checked:
                        valid_original = np.any(
                            transect.w_vel.valid_data[1, :, transect.in_transect_idx].T,
                            0,
                        )

                        # Determine what data each filter have marked
                        # invalid. Original invalid data are excluded
                        valid = np.any(
                            transect.w_vel.valid_data[
                                filter_idx, :, transect.in_transect_idx
                            ].T,
                            0,
                        )
                        if filter_idx > 1:
                            valid_int = valid.astype(int) - valid_original.astype(int)
                            valid = valid_int != -1

                        # Check if all data are invalid
                        if filter_idx == 0:
                            if np.nansum(valid.astype(int)) < 1:
                                self.w_vel["all_invalid"][n] = True
                        # TODO seems like the rest of this should be under
                        #  else of all invalid or multiple messages generated.

                        # Compute characteristics
                        q_total, q_max_run, number_invalid_ens = QAData.invalid_qa(
                            valid, meas.discharge[n]
                        )
                        self.w_vel["q_total"][n, filter_idx] = q_total
                        self.w_vel["q_max_run"][n, filter_idx] = q_max_run

                        # Compute percentage compared to total
                        if meas.discharge[n].total == 0.0:
                            q_total_percent = np.nan
                            q_max_run_percent = np.nan
                        else:
                            q_total_percent = np.abs(
                                (q_total / meas.discharge[n].total) * 100
                            )
                            q_max_run_percent = np.abs(
                                (q_max_run / meas.discharge[n].total) * 100
                            )

                        # Check total invalid discharge in ensembles for
                        # warning
                        if q_total_percent > self.q_total_threshold_warning:
                            self.w_vel["q_total_warning"][n, filter_idx] = True

                        # Apply run or cluster thresholds
                        if q_max_run_percent > self.q_run_threshold_warning:
                            self.w_vel["q_max_run_warning"][n, filter_idx] = True
                        elif q_max_run_percent > self.q_run_threshold_caution:
                            self.w_vel["q_max_run_caution"][n, filter_idx] = True

                        # Compute percent discharge interpolated for both
                        # cells and ensembles
                        # This approach doesn't exclude original data
                        valid_cells = transect.w_vel.valid_data[
                            filter_idx, :, transect.in_transect_idx
                        ].T
                        q_invalid_total = (
                            np.nansum(
                                meas.discharge[n].middle_cells[
                                    np.logical_not(valid_cells)
                                ]
                            )
                            + np.nansum(
                                meas.discharge[n].top_ens[np.logical_not(valid)]
                            )
                            + np.nansum(
                                meas.discharge[n].bottom_ens[np.logical_not(valid)]
                            )
                        )
                        q_invalid_total_percent = (
                            q_invalid_total / meas.discharge[n].total
                        ) * 100

                        if q_invalid_total_percent > self.q_total_threshold_caution:
                            self.w_vel["q_total_caution"][n, filter_idx] = True

            # Generate messages for ensemble run or clusters
            if np.any(self.w_vel["q_max_run_warning"]):
                self.w_vel["messages"].append(
                    [
                        self.tr("WT: Int. Q for consecutive invalid ensembles exceeds ")
                        + "%3.0f" % self.q_run_threshold_warning + "%;",
                        1,
                        11,
                    ]
                )
                guidance_text = self.tr("More than 5% of the discharge was computed for consecutive ensembles with invalid water track data using interpolated velocities. Review the velocity distribution to verify that the interpolated values are reasonable. The SNR filter for SonTek data can cause considerable loss of data and should be evaluated with the filter on and off.")
                self.w_vel["guidance"].append(
                    self.guidance_prep(self.w_vel["messages"][-1][0], guidance_text))
                status_switch = 2

            elif np.any(self.w_vel["q_total_warning"]):
                self.w_vel["messages"].append(
                    [
                        self.tr("WT: Int. Q for invalid cells and ensembles in a transect exceeds ")
                        + "%3.0f" % self.q_total_threshold_warning
                        + "%;",
                        1,
                        11,
                    ]
                )
                guidance_text = self.tr("More than 25% of the discharge was computed for invalid water track data using interpolated velocities. Review the velocity distribution to verify that the interpolated values are reasonable. The SNR filter for SonTek data can cause considerable loss of data and should be evaluated with the filter on and off.")
                self.w_vel["guidance"].append(
                    self.guidance_prep(self.w_vel["messages"][-1][0], guidance_text))
                status_switch = 2

            elif np.any(self.w_vel["q_max_run_caution"]):
                self.w_vel["messages"].append(
                    [
                        self.tr("wt: Int. Q for consecutive invalid ensembles exceeds ") 
                        + "%3.0f" % self.q_run_threshold_caution + "%;",
                        2,
                        11,
                    ]
                )
                guidance_text = self.tr("More than 3% of the discharge was computed for consecutive ensembles with invalid water track data using interpolated velocities. Review the velocity distribution to verify that the interpolated values are reasonable. The SNR filter for SonTek data can cause considerable loss of data and should be evaluated with the filter on and off.")
                self.w_vel["guidance"].append(
                    self.guidance_prep(self.w_vel["messages"][-1][0], guidance_text))
                if status_switch < 1:
                    status_switch = 1

            elif np.any(self.w_vel["q_total_caution"]):
                self.w_vel["messages"].append(
                    [
                        self.tr("wt: Int. Q for invalid cells and ensembles in a transect exceeds ")
                        + "%3.0f" % self.q_total_threshold_caution
                        + "%;",
                        2,
                        11,
                    ]
                )
                guidance_text = self.tr("More than 10% of the discharge was computed for invalid water track data using interpolated velocities. Review the velocity distribution to verify that the interpolated values are reasonable. The SNR filter for SonTek data can cause considerable loss of data and should be evaluated with the filter on and off.")
                self.w_vel["guidance"].append(
                    self.guidance_prep(self.w_vel["messages"][-1][0], guidance_text))
                if status_switch < 1:
                    status_switch = 1

            # Generate message for all invalid
            if np.any(self.w_vel["all_invalid"]):
                self.w_vel["messages"].append(
                    [
                        self.tr("WT: There are no valid data for one or more transects") + ";",
                        1,
                        11,
                    ]
                )
                guidance_text = self.tr("There are no valid water velocities. Check the filters to make sure the settings are reasonable. If the filter settings are good, then remove the transect from consideration in the computation of discharge. The SNR filter for SonTek data can cause considerable loss of data and should be evaluated with the filter on and off.")
                self.w_vel["guidance"].append(
                    self.guidance_prep(self.w_vel["messages"][-1][0], guidance_text))
                status_switch = 2

            # Set status
            self.w_vel["status"] = "good"
            if status_switch == 2:
                self.w_vel["status"] = "warning"
            elif status_switch == 1:
                self.w_vel["status"] = "caution"
        else:
            self.w_vel["status"] = "inactive"

    def extrapolation_qa(self, meas):
        """Apply quality checks to extrapolation methods

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        self.extrapolation["messages"] = []
        self.extrapolation["guidance"] = []

        checked = []
        discharges = []
        for n, transect in enumerate(meas.transects):
            checked.append(transect.checked)
            if transect.checked:
                discharges.append(meas.discharge[n])

        if any(checked):
            self.extrapolation["status"] = "good"
            extrap_uncertainty = Uncertainty.uncertainty_extrapolation(meas, discharges)

            if np.abs(extrap_uncertainty) > 2:
                self.extrapolation["messages"].append(
                    [
                        self.tr("Extrapolation: The extrapolation uncertainty is more than 2 percent") + ";",
                        2,
                        12,
                    ]
                )
                guidance_text = self.tr("The extrapolation method will have a substantial effect on the discharge. Carefully evaluate various extrapolation methods and choose the method that best fits the data. Provide justification for the selected method in the comments.")
                self.extrapolation["guidance"].append(
                    self.guidance_prep(self.extrapolation["messages"][-1][0], guidance_text))
                self.extrapolation["status"] = "caution"
        else:
            self.extrapolation["status"] = "inactive"

    def edges_qa(self, meas):
        """Apply quality checks to edge estimates

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        # Initialize variables
        self.edges["messages"] = []
        self.edges["guidance"] = []
        checked = []
        left_q = []
        right_q = []
        total_q = []
        edge_dist_left = []
        edge_dist_right = []
        dist_moved_left = []
        dist_moved_right = []
        dist_made_good = []
        left_type = []
        right_type = []
        transect_idx = []

        for n, transect in enumerate(meas.transects):
            checked.append(transect.checked)

            if transect.checked:
                left_q.append(meas.discharge[n].left)
                right_q.append(meas.discharge[n].right)
                total_q.append(meas.discharge[n].total)
                dmr, dml, dmg = QAData.edge_distance_moved(transect)
                dist_moved_right.append(dmr)
                dist_moved_left.append(dml)
                dist_made_good.append(dmg)
                edge_dist_left.append(transect.edges.left.distance_m)
                edge_dist_right.append(transect.edges.right.distance_m)
                left_type.append(transect.edges.left.type)
                right_type.append(transect.edges.right.type)
                transect_idx.append(n)

        if any(checked):
            # Set default status to good
            self.edges["status"] = "good"

            mean_total_q = np.nanmean(total_q)

            # Check left edge q > 5%
            self.edges["left_q"] = 0

            left_q_percent = (np.nanmean(left_q) / mean_total_q) * 100
            temp_idx = np.where(left_q / mean_total_q > 0.05)[0]

            if len(temp_idx) > 0:
                self.edges["left_q_idx"] = np.array(transect_idx)[temp_idx]
            else:
                self.edges["left_q_idx"] = []

            if np.abs(left_q_percent) > 5:
                self.edges["left_q"] = 1
            elif len(self.edges["left_q_idx"]) > 0:
                self.edges["left_q"] = 1

            # Check right edge q > 5%
            self.edges["right_q"] = 0
            right_q_percent = (np.nanmean(right_q) / mean_total_q) * 100
            temp_idx = np.where(right_q / mean_total_q > 0.05)[0]
            if len(temp_idx) > 0:
                self.edges["right_q_idx"] = np.array(transect_idx)[temp_idx]
            else:
                self.edges["right_q_idx"] = []
            if np.abs(right_q_percent) > 5:
                self.edges["right_q"] = 1
            elif len(self.edges["right_q_idx"]) > 0:
                self.edges["right_q"] = 1

            # Edge Q message
            if np.abs(right_q_percent) > 5 or np.abs(left_q_percent) > 5:
                self.edges["status"] = "caution"
                self.edges["messages"].append(
                    [self.tr("Edges: Edge Q is greater than 5%") + ";", 1, 13]
                )
                guidance_text = self.tr("It is recommended that an edge contain a maximum of 5% of the total discharge. If an edge contains more than 5% consider using a different cross section, if possible. If it is not possible, provide documentation, including if the edge discharge appears reasonable.")
                self.edges["guidance"].append(
                    self.guidance_prep(self.edges["messages"][-1][0], guidance_text))
            elif (
                len(self.edges["right_q_idx"]) > 0 or len(self.edges["left_q_idx"]) > 0
            ):
                self.edges["status"] = "caution"
                self.edges["messages"].append(
                    [
                        self.tr("Edges: One or more transects have an edge edge Q greater than 5%") + ";",
                        1,
                        13,
                    ]
                )
                guidance_text = self.tr("It is recommended that an edge contain a maximum of 5% of the total discharge. If an edge contains more than 5% consider using a different cross section, if possible. If it is not possible, provide documentation, including if the edge discharge appears reasonable.")
                self.edges["guidance"].append(
                    self.guidance_prep(self.edges["messages"][-1][0], guidance_text))

            # Check for consistent sign
            q_positive = []
            self.edges["left_sign"] = 0
            for q in left_q:
                if q >= 0:
                    q_positive.append(True)
                else:
                    q_positive.append(False)
            if len(np.unique(q_positive)) > 1 and left_q_percent > 0.5:
                self.edges["left_sign"] = 1

            q_positive = []
            self.edges["right_sign"] = 0
            for q in right_q:
                if q >= 0:
                    q_positive.append(True)
                else:
                    q_positive.append(False)
            if len(np.unique(q_positive)) > 1 and right_q_percent > 0.5:
                self.edges["right_sign"] = 1

            if self.edges["right_sign"] or self.edges["left_sign"]:
                self.edges["status"] = "caution"
                self.edges["messages"].append(
                    [self.tr("Edges: Sign of edge Q is not consistent") + ";", 2, 13]
                )
                guidance_text = self.tr("The sign of the discharge for an edge is not consistent, as expected. This may happen if the velocities are very low and there is flow fluctuation. This could also happen if there is a flow reversal on the edge and the transect is not started or stopped in a consistent location. If there is negative flow in the edge that flow should be captured in the transect.")
                self.edges["guidance"].append(
                    self.guidance_prep(self.edges["messages"][-1][0], guidance_text))

            # Check distance moved
            dmg_5_percent = 0.05 * np.nanmean(dist_made_good)
            avg_right_edge_dist = np.nanmean(edge_dist_right)
            right_threshold = np.nanmin([dmg_5_percent, avg_right_edge_dist])
            temp_idx = np.where(dist_moved_right > right_threshold)[0]
            if len(temp_idx) > 0:
                self.edges["right_dist_moved_idx"] = np.array(transect_idx)[temp_idx]
            else:
                self.edges["right_dist_moved_idx"] = []

            avg_left_edge_dist = np.nanmean(edge_dist_left)
            left_threshold = np.nanmin([dmg_5_percent, avg_left_edge_dist])
            temp_idx = np.where(dist_moved_left > left_threshold)[0]
            if len(temp_idx) > 0:
                self.edges["left_dist_moved_idx"] = np.array(transect_idx)[temp_idx]
            else:
                self.edges["left_dist_moved_idx"] = []

            # Excessive movement message
            if (
                len(self.edges["right_dist_moved_idx"]) > 0
                or len(self.edges["left_dist_moved_idx"]) > 0
            ):
                self.edges["status"] = "caution"
                self.edges["messages"].append(
                    [self.tr("Edges: Excessive boat movement in edge ensembles") + ";", 2, 13]
                )
                guidance_text = self.tr("Data used to compute the edge discharge should be collected in a fixed location and the distance from that location to the water's edge should be measured. Excessive movement of the boat during the collection of ensembles used to compute the edge discharge may result in inaccurate average depth and velocity used in the edge discharge computation. Verify the ensembles used in the edge computation.")
                self.edges["guidance"].append(
                    self.guidance_prep(self.edges["messages"][-1][0], guidance_text))

            # Check for edge ensembles marked invalid due to excluded distance
            self.edges["invalid_transect_left_idx"] = []
            self.edges["invalid_transect_right_idx"] = []
            for n, transect in enumerate(meas.transects):
                if transect.checked:
                    ens_invalid = np.nansum(transect.w_vel.valid_data[0, :, :], 0) > 0
                    ens_cells_above_sl = np.nansum(transect.w_vel.cells_above_sl, 0) > 0
                    ens_invalid = np.logical_not(
                        np.logical_and(ens_invalid, ens_cells_above_sl)
                    )
                    if np.any(ens_invalid):
                        if transect.start_edge == "Left":
                            invalid_left = ens_invalid[
                                0 : int(transect.edges.left.number_ensembles)
                            ]
                            invalid_right = ens_invalid[
                                -int(transect.edges.right.number_ensembles) :
                            ]
                        else:
                            invalid_right = ens_invalid[
                                0 : int(transect.edges.right.number_ensembles)
                            ]
                            invalid_left = ens_invalid[
                                -int(transect.edges.left.number_ensembles) :
                            ]
                        if len(invalid_left) > 0:
                            left_invalid_percent = sum(invalid_left) / len(invalid_left)
                        else:
                            left_invalid_percent = 0
                        if len(invalid_right) > 0:
                            right_invalid_percent = sum(invalid_right) / len(
                                invalid_right
                            )
                        else:
                            right_invalid_percent = 0
                        max_invalid_percent = (
                            max([left_invalid_percent, right_invalid_percent]) * 100
                        )
                        if max_invalid_percent > 25:
                            self.edges["status"] = "caution"
                            if np.any(left_invalid_percent > 0.25):
                                self.edges["invalid_transect_left_idx"].append(n)
                            if np.any(right_invalid_percent > 0.25):
                                self.edges["invalid_transect_right_idx"].append(n)

            if (
                len(self.edges["invalid_transect_left_idx"]) > 0
                or len(self.edges["invalid_transect_right_idx"]) > 0
            ):
                self.edges["messages"].append(
                    [
                        self.tr("Edges: The percent of invalid ensembles exceeds 25% in one or more transects") + ";",
                        2,
                        13,
                    ]
                )
                guidance_text = self.tr("More than 25% of the ensembles used to compute the mean depth and velocity for edge discharge computations are invalid. Verify the ensembles used in the edge computation provide a good average depth and velocity. If more ensembles are needed change the number of edge ensembles and adjust the distance to the water's edge as appropriate.")
                self.edges["guidance"].append(
                    self.guidance_prep(self.edges["messages"][-1][0], guidance_text))

            # Check edges for zero discharge
            self.edges["left_zero"] = 0
            temp_idx = np.where(np.round(left_q, 4) == 0)[0]
            if len(temp_idx) > 0:
                self.edges["left_zero_idx"] = np.array(transect_idx)[temp_idx]
                self.edges["left_zero"] = 2
            else:
                self.edges["left_zero_idx"] = []

            self.edges["right_zero"] = 0
            temp_idx = np.where(np.round(right_q, 4) == 0)[0]
            if len(temp_idx) > 0:
                self.edges["right_zero_idx"] = np.array(transect_idx)[temp_idx]
                self.edges["right_zero"] = 2
            else:
                self.edges["right_zero_idx"] = []

            # Zero Q Message
            if self.edges["right_zero"] == 2 or self.edges["left_zero"] == 2:
                self.edges["status"] = "warning"
                self.edges["messages"].append([self.tr("EDGES: Edge has zero Q") + ";", 1, 13])
                guidance_text = self.tr("Edges typically do not have a zero discharge. Check that the edge distance has been entered and there are sufficient valid ensembles to compute a mean depth and velocity. Provide documentation for any needed changes.")
                self.edges["guidance"].append(
                    self.guidance_prep(self.edges["messages"][-1][0], guidance_text))

            # Check consistent edge type
            self.edges["left_type"] = 0
            if len(np.unique(left_type)) > 1:
                self.edges["left_type"] = 2

            self.edges["right_type"] = 0
            if len(np.unique(right_type)) > 1:
                self.edges["right_type"] = 2

            # Inconsistent type message
            if self.edges["right_type"] == 2 or self.edges["left_type"] == 2:
                self.edges["status"] = "warning"
                self.edges["messages"].append(
                    [self.tr("EDGES: An edge has an inconsistent edge type") + ";", 1, 13]
                )
                guidance_text = self.tr("Unless the transects have been collected at different cross sections the edge type should be consistent for all transects in a measurement. Adjust the edge type as appropriate and provide documentation to explain the difference or needed change.")
                self.edges["guidance"].append(
                    self.guidance_prep(self.edges["messages"][-1][0], guidance_text))

        else:
            self.edges["status"] = "inactive"

    @staticmethod
    def invalid_qa(valid, discharge):
        """Computes the total invalid discharge in ensembles that have
        invalid data. The function also computes the maximum run or cluster
        of ensembles with the maximum interpolated discharge.

        Parameters
        ----------
        valid: np.array(bool)
            Array identifying valid and invalid ensembles.
        discharge: QComp
            Object of class QComp

        Returns
        -------
        q_invalid_total: float
            Total interpolated discharge in invalid ensembles
        q_invalid_max_run: float
            Maximum interpolated discharge in a run or cluster of invalid
            ensembles
        ens_invalid: int
            Total number of invalid ensembles
        """

        # Create bool for invalid data
        invalid = np.logical_not(valid)
        q_invalid_total = (
            np.nansum(discharge.middle_ens[invalid])
            + np.nansum(discharge.top_ens[invalid])
            + np.nansum(discharge.bottom_ens[invalid])
        )

        # Compute total number of invalid ensembles
        ens_invalid = np.sum(invalid)

        # Compute the indices of where changes occur
        valid_int = np.insert(valid.astype(int), 0, -1)
        valid_int = np.append(valid_int, -1)
        valid_run = np.where(np.diff(valid_int) != 0)[0]
        run_length = np.diff(valid_run)
        run_length0 = run_length[(valid[0] == 1) :: 2]

        n_runs = len(run_length0)

        if valid[0]:
            n_start = 1
        else:
            n_start = 0

        n_end = len(valid_run) - 1

        if n_runs > 0:
            m = 0
            q_invalid_run = []
            for n in range(n_start, n_end, 2):
                m += 1
                idx_start = valid_run[n]
                idx_end = valid_run[n + 1]
                q_invalid_run.append(
                    np.nansum(discharge.middle_ens[idx_start:idx_end])
                    + np.nansum(discharge.top_ens[idx_start:idx_end])
                    + np.nansum(discharge.bottom_ens[idx_start:idx_end])
                )

            # Determine the maximum discharge in a single run
            q_invalid_max_run = np.nanmax(np.abs(q_invalid_run))

        else:
            q_invalid_max_run = 0.0

        return q_invalid_total, q_invalid_max_run, ens_invalid

    @staticmethod
    def edge_distance_moved(transect):
        """Computes the boat movement during edge ensemble collection.

        Parameters
        ----------
        transect: Transect
            Object of class Transect

        Returns
        -------
        right_dist_moved: float
            Distance in m moved during collection of right edge samples
        left_dist_moved: float
            Distance in m moved during collection of left edge samples
        dmg: float
            Distance made good for the entire transect
        """

        boat_selected = getattr(transect.boat_vel, transect.boat_vel.selected)
        ens_duration = transect.date_time.ens_duration_sec

        # Get boat velocities
        if boat_selected is not None:
            u_processed = boat_selected.u_processed_mps
            v_processed = boat_selected.v_processed_mps
        else:
            u_processed = np.tile(
                np.nan, transect.boat_vel.bt_vel.u_processed_mps.shape
            )
            v_processed = np.tile(
                np.nan, transect.boat_vel.bt_vel.v_processed_mps.shape
            )

        # Compute boat coordinates
        x_processed = np.nancumsum(u_processed * ens_duration)
        y_processed = np.nancumsum(v_processed * ens_duration)
        dmg = (x_processed[-1] ** 2 + y_processed[-1] ** 2) ** 0.5

        # Compute left distance moved
        # TODO should be a dist moved function
        left_edge_idx = QComp.edge_ensembles("left", transect)
        if len(left_edge_idx) > 0:
            boat_x = x_processed[left_edge_idx[-1]] - x_processed[left_edge_idx[0]]
            boat_y = y_processed[left_edge_idx[-1]] - y_processed[left_edge_idx[0]]
            left_dist_moved = (boat_x**2 + boat_y**2) ** 0.5
        else:
            left_dist_moved = np.nan

        # Compute right distance moved
        right_edge_idx = QComp.edge_ensembles("right", transect)
        if len(right_edge_idx) > 0:
            boat_x = x_processed[right_edge_idx[-1]] - x_processed[right_edge_idx[0]]
            boat_y = y_processed[right_edge_idx[-1]] - y_processed[right_edge_idx[0]]
            right_dist_moved = (boat_x**2 + boat_y**2) ** 0.5
        else:
            right_dist_moved = np.nan

        return right_dist_moved, left_dist_moved, dmg

    # check for user changes
    def check_bt_setting(self, meas):
        """Checks the bt settings to see if they are still on the default
        settings.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        self.settings_dict["tab_bt"] = "Default"

        s = meas.current_settings()
        d = meas.qrev_default_settings()

        if s["BTbeamFilter"] != d["BTbeamFilter"]:
            self.bt_vel["messages"].append(
                [self.tr("BT: User modified default beam setting") + ";", 3, 8]
            )
            guidance_text = self.tr("This filter determines the use of 3-beam solutions for bottom track. Provide documentation on the logic for changing the default filter setting.")
            self.bt_vel["guidance"].append(
                self.guidance_prep(self.bt_vel["messages"][-1][0], guidance_text))
            self.settings_dict["tab_bt"] = "Custom"

        if s["BTdFilter"] != d["BTdFilter"]:
            self.bt_vel["messages"].append(
                [self.tr("BT: User modified default error velocity filter") + ";", 3, 8]
            )
            guidance_text = self.tr("This filter is based on an assumption of a random distribution of the error velocity. For SonTek data, each frequency is treated separately. Provide documentation on the logic for changing the default filter setting.")
            self.bt_vel["guidance"].append(
                self.guidance_prep(self.bt_vel["messages"][-1][0], guidance_text))
            self.settings_dict["tab_bt"] = "Custom"
            self.settings_dict["tab_bt"] = "Custom"
            self.settings_dict["tab_bt"] = "Custom"

        if s["BTwFilter"] != d["BTwFilter"]:
            self.bt_vel["messages"].append(
                [self.tr("BT: User modified default vertical velocity filter") + ";", 3, 8]
            )
            guidance_text = self.tr("This filter is based on an assumption of a random distribution of the vertical velocity. For SonTek data, each frequency is treated separately. Provide documentation on the logic for changing the default filter setting.")
            self.bt_vel["guidance"].append(
                self.guidance_prep(self.bt_vel["messages"][-1][0], guidance_text))
            self.settings_dict["tab_bt"] = "Custom"
            self.settings_dict["tab_bt"] = "Custom"
            self.settings_dict["tab_bt"] = "Custom"

        if s["BTsmoothFilter"] != d["BTsmoothFilter"]:
            self.bt_vel["messages"].append(
                [self.tr("BT: User modified default smooth filter") + ";", 3, 8]
            )
            guidance_text = self.tr("The smooth filter is not used by default. It can be used when the standard 3-beam, error velocity, and vertical velocity filters fail to mark obvious spikes in the boat velocity invalid. Provide documentation on the logic for changing the default filter setting.")
            self.bt_vel["guidance"].append(
                self.guidance_prep(self.bt_vel["messages"][-1][0], guidance_text))
            self.settings_dict["tab_bt"] = "Custom"
            self.settings_dict["tab_bt"] = "Custom"
            self.settings_dict["tab_bt"] = "Custom"

    def check_wt_settings(self, meas):
        """Checks the wt settings to see if they are still on the default
        settings.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        self.settings_dict["tab_wt"] = "Default"

        s = meas.current_settings()
        d = meas.qrev_default_settings()

        if round(s["WTExcludedDistance"], 2) != round(d["WTExcludedDistance"], 2):
            self.w_vel["messages"].append(
                [self.tr("WT: User modified excluded distance") + ";", 3, 11]
            )
            guidance_text = self.tr("The excluded distance is set in QRev to remove the potential low bias caused by flow disturbance. Reducing the excluded distance could result in a low bias in the cells near the transducer. Provide documentation to justify the setting.")
            self.w_vel["guidance"].append(
                self.guidance_prep(self.w_vel["messages"][-1][0], guidance_text))
            self.settings_dict["tab_wt"] = "Custom"

        if s["WTbeamFilter"] != d["WTbeamFilter"]:
            self.w_vel["messages"].append(
                [self.tr("WT: User modified default beam setting") + ";", 3, 11]
            )
            guidance_text = self.tr("This filter determines the use of 3-beam solutions for bottom track. Provide documentation for the logic of changing the default filter.")
            self.w_vel["guidance"].append(
                self.guidance_prep(self.w_vel["messages"][-1][0], guidance_text))
            self.settings_dict["tab_wt"] = "Custom"

        if s["WTdFilter"] != d["WTdFilter"]:
            self.w_vel["messages"].append(
                [self.tr("WT: User modified default error velocity filter") + ";", 3, 11]
            )
            guidance_text = self.tr("This filter is based on an assumption of a random distribution of the error velocity. When set to Auto each ping type is treated separately. A manually provided value is applied to all ping types. Provide documentation for the logic of changing the default filter.")
            self.w_vel["guidance"].append(
                self.guidance_prep(self.w_vel["messages"][-1][0], guidance_text))
            self.settings_dict["tab_wt"] = "Custom"

        if s["WTwFilter"] != d["WTwFilter"]:
            self.w_vel["messages"].append(
                [self.tr("WT: User modified default vertical velocity filter") + ";", 3, 11]
            )
            guidance_text = self.tr("This filter is based on an assumption of a random distribution of the vertical velocity. When set to Auto each ping type is treated separately. A manually provided value is applied to all ping types. Provide documentation for the logic of changing the default filter.")
            self.w_vel["guidance"].append(
                self.guidance_prep(self.w_vel["messages"][-1][0], guidance_text))
            self.settings_dict["tab_wt"] = "Custom"

        if s["WTsnrFilter"] != d["WTsnrFilter"]:
            self.w_vel["messages"].append(
                [self.tr("WT: User modified default SNR filter") + ";", 3, 11]
            )
            guidance_text = self.tr("The SNR filter for SonTek data can cause considerable loss of data and should be evaluated with the filter on and off. Provide documentation for the logic of changing the default filter.")
            self.w_vel["guidance"].append(
                self.guidance_prep(self.w_vel["messages"][-1][0], guidance_text))
            self.settings_dict["tab_wt"] = "Custom"

    def check_extrap_settings(self, meas):
        """Checks the extrap to see if they are still on the default
        settings.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        self.settings_dict["tab_extrap"] = "Default"

        # Check fit parameters
        if meas.extrap_fit.sel_fit[0].fit_method != "Automatic":
            self.settings_dict["tab_extrap"] = "Custom"
            self.extrapolation["messages"].append(
                [self.tr("Extrapolation: User modified default automatic setting") + ";", 3, 12]
            )
            guidance_text = self.tr("The extrapolation has been set manually. Provide justification for the selected method in the comments.")
            self.extrapolation["guidance"].append(
                self.guidance_prep(self.extrapolation["messages"][-1][0], guidance_text))

        # Check data parameters
        if meas.extrap_fit.sel_fit[-1].data_type.lower() != "q":
            self.settings_dict["tab_extrap"] = "Custom"
            self.extrapolation["messages"].append(
                [self.tr("Extrapolation: User modified data type") + ";", 3, 12]
            )
            guidance_text = self.tr("It is not recommend to use velocity as the data type when evaluating the extrapolation methods for use in the moving-boat discharge measurement.")
            self.extrapolation["guidance"].append(
                self.guidance_prep(self.extrapolation["messages"][-1][0], guidance_text))

        if meas.extrap_fit.threshold != 20:
            self.settings_dict["tab_extrap"] = "Custom"
            self.extrapolation["messages"].append(
                [self.tr("Extrapolation: User modified default threshold") + ";", 3, 12]
            )
            guidance_text = self.tr("Changing the default threshold can be used to remove a median value from consideration that has fewer points in the median. Provide justification for the threshold setting in the documentation.")
            self.extrapolation["guidance"].append(
                self.guidance_prep(self.extrapolation["messages"][-1][0], guidance_text))

        if meas.extrap_fit.subsection[0] != 0 or meas.extrap_fit.subsection[1] != 100:
            self.settings_dict["tab_extrap"] = "Custom"
            self.extrapolation["messages"].append(
                [self.tr("Extrapolation: User modified subsectioning") + ";", 3, 12]
            )
            guidance_text = self.tr("Changing the default subsectioning can be used to remove the influence of data near the stream banks. Provide justification for the subsectioning in the documentation.")
            self.extrapolation["guidance"].append(
                self.guidance_prep(self.extrapolation["messages"][-1][0], guidance_text))

    def check_tempsal_settings(self, meas):
        """Checks the temp and salinity settings to see if they are still on
        the default settings.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        self.settings_dict["tab_tempsal"] = "Default"

        t_source_change = False
        salinity_change = False
        s_sound_change = False
        t_user_change = False
        t_adcp_change = False

        if not all(
            np.isnan([meas.ext_temp_chk["user"], meas.ext_temp_chk["user_orig"]])
        ):
            if meas.ext_temp_chk["user"] != meas.ext_temp_chk["user_orig"]:
                t_user_change = True

        if not all(
            np.isnan([meas.ext_temp_chk["adcp"], meas.ext_temp_chk["adcp_orig"]])
        ):
            if meas.ext_temp_chk["adcp"] != meas.ext_temp_chk["adcp_orig"]:
                t_adcp_change = True

        # Check each checked transect
        for idx in meas.checked_transect_idx:
            transect = meas.transects[idx]

            # Temperature source
            if transect.sensors.temperature_deg_c.selected != "internal":
                t_source_change = True

            if transect.sensors.salinity_ppt.selected != "internal":
                sal = getattr(
                    transect.sensors.salinity_ppt,
                    transect.sensors.salinity_ppt.selected,
                )
                if transect.sensors.salinity_ppt.internal is not None:
                    if np.all(
                        np.equal(sal.data, transect.sensors.salinity_ppt.internal.data)
                    ):
                        salinity_change = False
                    else:
                        salinity_change = True

            # Speed of Sound
            if transect.sensors.speed_of_sound_mps.selected != "internal":
                s_sound_change = True

        # Report condition and messages
        if any(
            [
                t_source_change,
                salinity_change,
                s_sound_change,
                t_adcp_change,
                t_user_change,
            ]
        ):
            self.settings_dict["tab_tempsal"] = "Custom"

            if t_source_change:
                self.temperature["messages"].append(
                    [self.tr("Temperature: User modified temperature source") + ";", 3, 5]
                )
                guidance_text = self.tr("The user has modified the temperature source. Validate the new data from field notes or other documentation.")
                self.temperature["guidance"].append(
                    self.guidance_prep(self.temperature["messages"][-1][0],
                                       guidance_text))

            if s_sound_change:
                self.temperature["messages"].append(
                    [self.tr("Temperature: User modified speed of sound source") + ";", 3, 5]
                )
                guidance_text = self.tr("The user has modified the speed of sound source. Validate the new data from field notes or other documentation.")
                self.temperature["guidance"].append(
                    self.guidance_prep(self.temperature["messages"][-1][0],
                                       guidance_text))

            if t_user_change:
                self.temperature["messages"].append(
                    [self.tr("Temperature: User modified independent temperature") + ";", 3, 5]
                )
                guidance_text = self.tr("The user has modified the independent temperature reading. Validate this reading from the field notes.")
                self.temperature["guidance"].append(
                    self.guidance_prep(self.temperature["messages"][-1][0],
                                       guidance_text))

            if t_adcp_change:
                self.temperature["messages"].append(
                    [self.tr("Temperature: User modified ADCP temperature") + ";", 3, 5]
                )
                guidance_text = self.tr("The user has modified the ADCP temperature. Validate this reading from the field notes and with the ADCP temperature time series.")
                self.temperature["guidance"].append(
                    self.guidance_prep(self.temperature["messages"][-1][0],
                                       guidance_text))

    def check_gps_settings(self, meas):
        """Checks the gps settings to see if they are still on the default
        settings.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        gps = False
        self.settings_dict["tab_gps"] = "Default"

        # Check for transects with gga or vtg data
        for idx in meas.checked_transect_idx:
            transect = meas.transects[idx]
            if (
                transect.boat_vel.gga_vel is not None
                or transect.boat_vel.gga_vel is not None
            ):
                gps = True
                break

        # If gga or vtg data exist check settings
        if gps:
            s = meas.current_settings()
            d = meas.qrev_default_settings()

            if s["ggaDiffQualFilter"] != d["ggaDiffQualFilter"]:
                self.gga_vel["messages"].append(
                    [self.tr("GPS: User modified default quality setting") + ";", 3, 8]
                )
                guidance_text = self.tr("If the quality setting is less than 2, evaluate the time series and shiptrack for large random errors. Provide documentation on the logic for changing the default filter setting.")
                self.gga_vel["guidance"].append(
                    self.guidance_prep(self.gga_vel["messages"][-1][0], guidance_text))
                self.settings_dict["tab_gps"] = "Custom"

            if s["ggaAltitudeFilter"] != d["ggaAltitudeFilter"]:
                self.gga_vel["messages"].append(
                    [self.tr("GPS: User modified default altitude filter") + ";", 3, 8]
                )
                guidance_text = self.tr("The altitude filter is well correlated with the horizontal accuracy of the GGA data. The default setting is based on a target of sub-meter accuracy. Increasing the altitude filter would allow less accurate data to be used. Provide documentation on the logic for changing the default filter setting.")
                self.gga_vel["guidance"].append(
                    self.guidance_prep(self.gga_vel["messages"][-1][0], guidance_text))
                self.settings_dict["tab_gps"] = "Custom"

            if s["GPSHDOPFilter"] != d["GPSHDOPFilter"]:
                self.gga_vel["messages"].append(
                    [self.tr("GPS: User modified default HDOP filter") + ";", 3, 8]
                )
                guidance_text = self.tr("The HDOP is a measure of accuracy based on satellite configuration. Increasing the value of HDOP could result in the acceptance of less accurate data. Provide documentation on the logic for changing the default filter setting.")
                self.gga_vel["guidance"].append(
                    self.guidance_prep(self.gga_vel["messages"][-1][0], guidance_text))
                self.settings_dict["tab_gps"] = "Custom"

            if s["GPSSmoothFilter"] != d["GPSSmoothFilter"]:
                self.gga_vel["messages"].append(
                    [self.tr("GPS: User modified default smooth filter") + ";", 3, 8]
                )
                guidance_text = self.tr("The smooth filter is not used by default. It can be used when the other filters fail to mark obvious spikes in the boat velocity invalid. Provide documentation on the logic for changing the default filter setting.")
                self.gga_vel["guidance"].append(
                    self.guidance_prep(self.gga_vel["messages"][-1][0], guidance_text))
                self.settings_dict["tab_gps"] = "Custom"

    def check_depth_settings(self, meas):
        """Checks the depth settings to see if they are still on the default
        settings.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        self.settings_dict["tab_depth"] = "Default"

        s = meas.current_settings()
        d = meas.qrev_default_settings()

        if s["depthReference"] != d["depthReference"]:
            self.depths["messages"].append(
                [self.tr("Depths: User modified depth reference") + ";", 3, 10]
            )
            guidance_text = self.tr("Provide documentation to justify the selected depth reference.")
            self.depths["guidance"].append(
                self.guidance_prep(self.depths["messages"][-1][0], guidance_text))
            self.settings_dict["tab_depth"] = "Custom"

        if s["depthComposite"] != d["depthComposite"]:
            self.depths["messages"].append(
                [self.tr("Depths: User modified depth reference") + ";", 3, 10]
            )
            guidance_text = self.tr("Provide documentation to justify the selected depth reference.")
            self.depths["guidance"].append(
                self.guidance_prep(self.depths["messages"][-1][0], guidance_text))
            self.settings_dict["tab_depth"] = "Custom"

        if s["depthAvgMethod"] != d["depthAvgMethod"]:
            self.depths["messages"].append(
                [self.tr("Depths: User modified averaging method") + ";", 3, 10]
            )
            guidance_text = self.tr("The default inverse distance weighting method was changed to a simple average. Provide documentation to justify the change.")
            self.depths["guidance"].append(
                self.guidance_prep(self.depths["messages"][-1][0], guidance_text))
            self.settings_dict["tab_depth"] = "Custom"

        if s["depthFilterType"] != d["depthFilterType"]:
            self.depths["messages"].append(
                [self.tr("Depths: User modified filter type") + ";", 3, 10]
            )
            guidance_text = self.tr("The default filter was changed. Provide documentation to justify the change.")
            self.depths["guidance"].append(
                self.guidance_prep(self.depths["messages"][-1][0], guidance_text))
            self.settings_dict["tab_depth"] = "Custom"

        for idx in meas.checked_transect_idx:
            transect = meas.transects[idx]
            if (
                transect.depths.bt_depths.draft_orig_m
                != transect.depths.bt_depths.draft_use_m
            ):
                self.depths["messages"].append(
                    [self.tr("Depths: User modified draft") + ";", 3, 10]
                )
                guidance_text = self.tr("The depth of the transducer (draft) was changed. Provide documentation to justify the change.")
                self.depths["guidance"].append(
                    self.guidance_prep(self.depths["messages"][-1][0], guidance_text))
                self.settings_dict["tab_depth"] = "Custom"
                break

    def check_edge_settings(self, meas):
        """Checks the edge settings to see if they are still on the original
        settings.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        start_edge_change = False
        left_edge_type_change = False
        left_edge_dist_change = False
        left_edge_ens_change = False
        left_edge_q_change = False
        left_edge_coef_change = False
        right_edge_type_change = False
        right_edge_dist_change = False
        right_edge_ens_change = False
        right_edge_q_change = False
        right_edge_coef_change = False

        for idx in meas.checked_transect_idx:
            transect = meas.transects[idx]

            if transect.start_edge != transect.orig_start_edge:
                start_edge_change = True

            if transect.edges.left.type != transect.edges.left.orig_type:
                left_edge_type_change = True

            if transect.edges.left.distance_m != transect.edges.left.orig_distance_m:
                left_edge_dist_change = True

            if (
                transect.edges.left.number_ensembles
                != transect.edges.left.orig_number_ensembles
            ):
                left_edge_ens_change = True

            if (
                transect.edges.left.user_discharge_cms
                != transect.edges.left.orig_user_discharge_cms
            ):
                left_edge_q_change = True

            if transect.edges.left.cust_coef != transect.edges.left.orig_cust_coef:
                left_edge_coef_change = True

            if transect.edges.right.type != transect.edges.right.orig_type:
                right_edge_type_change = True

            if transect.edges.right.distance_m != transect.edges.right.orig_distance_m:
                right_edge_dist_change = True

            if (
                transect.edges.right.number_ensembles
                != transect.edges.right.orig_number_ensembles
            ):
                right_edge_ens_change = True

            if (
                transect.edges.right.user_discharge_cms
                != transect.edges.right.orig_user_discharge_cms
            ):
                right_edge_q_change = True

            if transect.edges.right.cust_coef != transect.edges.right.orig_cust_coef:
                right_edge_coef_change = True

        if any(
            [
                start_edge_change,
                left_edge_type_change,
                left_edge_dist_change,
                left_edge_ens_change,
                left_edge_q_change,
                left_edge_coef_change,
                right_edge_type_change,
                right_edge_dist_change,
                right_edge_ens_change,
                right_edge_q_change,
                right_edge_coef_change,
            ]
        ):
            self.settings_dict["tab_edges"] = "Custom"

            if start_edge_change:
                self.edges["messages"].append(
                    [self.tr("Edges: User modified start edge") + ";", 3, 10]
                )
                guidance_text = self.tr("Verify that the sign of the total discharge is correct and provide documentation of any changes.")
                self.edges["guidance"].append(
                    self.guidance_prep(self.edges["messages"][-1][0], guidance_text))
            if left_edge_type_change:
                self.edges["messages"].append(
                    [self.tr("Edges: User modified left edge type") + ";", 3, 10]
                )
                guidance_text = self.tr("Verify that the edge types are consistent and/or correct and provide documentation for the difference or any changes.")
                self.edges["guidance"].append(
                    self.guidance_prep(self.edges["messages"][-1][0], guidance_text))
            if left_edge_dist_change:
                self.edges["messages"].append(
                    [self.tr("Edges: User modified left edge distance") + ";", 3, 10]
                )
                guidance_text = self.tr("Document the reason for changing the edge distance, such as, incorrect value entered or a change due to changing the number of edge ensembles.")
                self.edges["guidance"].append(
                    self.guidance_prep(self.edges["messages"][-1][0], guidance_text))
            if left_edge_ens_change:
                self.edges["messages"].append(
                    [self.tr("Edges: User modified left number of ensembles") + ";", 3, 10]
                )
                guidance_text = self.tr("Document the reason for changing the number of ensembles and adjust the edge distance as appropriate.")
                self.edges["guidance"].append(
                    self.guidance_prep(self.edges["messages"][-1][0], guidance_text))
            if left_edge_q_change:
                self.edges["messages"].append(
                    [self.tr("Edges: User modified left user discharge") + ";", 3, 10]
                )
                guidance_text = self.tr("Provide documentation to support the specified user discharge.")
                self.edges["guidance"].append(
                    self.guidance_prep(self.edges["messages"][-1][0], guidance_text))
            if left_edge_coef_change:
                self.edges["messages"].append(
                    [self.tr("Edges: User modified left custom coefficient") + ";", 3, 10]
                )
                guidance_text = self.tr("Document the logic used to obtain the custom edge coefficient.")
                self.edges["guidance"].append(
                    self.guidance_prep(self.edges["messages"][-1][0], guidance_text))
            if right_edge_type_change:
                self.edges["messages"].append(
                    [self.tr("Edges: User modified right edge type") + ";", 3, 10]
                )
                guidance_text = self.tr("Verify that the edge types are consistent and/or correct and provide documentation for the difference or any changes.")
                self.edges["guidance"].append(
                    self.guidance_prep(self.edges["messages"][-1][0], guidance_text))
            if right_edge_dist_change:
                self.edges["messages"].append(
                    [self.tr("Edges: User modified right edge distance") + ";", 3, 10]
                )
                guidance_text = self.tr("Document the reason for changing the edge distance, such as, incorrect value entered or a change due to changing the number of edge ensembles.")
                self.edges["guidance"].append(
                    self.guidance_prep(self.edges["messages"][-1][0], guidance_text))
            if right_edge_ens_change:
                self.edges["messages"].append(
                    [self.tr("Edges: User modified right number of ensembles") + ";", 3, 10]
                )
                guidance_text = self.tr("Document the reason for changing the number of ensembles and adjust the edge distance as appropriate.")
                self.edges["guidance"].append(
                    self.guidance_prep(self.edges["messages"][-1][0], guidance_text))
            if right_edge_q_change:
                self.edges["messages"].append(
                    [self.tr("Edges: User modified right user discharge") + ";", 3, 10]
                )
                guidance_text = self.tr("Provide documentation to support the specified user discharge.")
                self.edges["guidance"].append(
                    self.guidance_prep(self.edges["messages"][-1][0], guidance_text))
            if right_edge_coef_change:
                self.edges["messages"].append(
                    [self.tr("Edges: User modified right custom coefficient") + ";", 3, 10]
                )
                guidance_text = self.tr("Document the logic used to obtain the custom edge coefficient.")
                self.edges["guidance"].append(
                    self.guidance_prep(self.edges["messages"][-1][0], guidance_text))
        else:
            self.settings_dict["tab_edges"] = "Default"

    def check_mbt_settings(self, meas):
        """Checks the mbt settings to see if they are still on the original
        settings.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        # if there are mb tests check for user changes
        if len(meas.mb_tests) >= 1:
            mbt = meas.mb_tests

            mb_user_valid = []
            mb_used = []

            auto = copy.deepcopy(mbt)
            auto = MovingBedTests.auto_use_2_correct(auto)

            for n in range(len(mbt)):
                if mbt[n].user_valid:
                    mb_user_valid.append(False)
                else:
                    mb_user_valid.append(True)

                if (
                    mbt[n].use_2_correct != auto[n].use_2_correct
                    and meas.transects[meas.checked_transect_idx[0]].boat_vel.selected
                    == "bt_vel"
                ):
                    mb_used.append(True)
                else:
                    mb_used.append(False)

            self.settings_dict["tab_mbt"] = "Default"
            if any(mb_user_valid):
                self.settings_dict["tab_mbt"] = "Custom"
                self.movingbed["messages"].append(
                    [self.tr("Moving-Bed Test: User modified valid test settings") + ";", 3, 6]
                )
                guidance_text = self.tr("Provide documentation as to why the test has been determined to be valid or invalid.")
                self.movingbed["guidance"].append(
                    self.guidance_prep(self.movingbed["messages"][-1][0], guidance_text))
            if any(mb_used):
                self.settings_dict["tab_mbt"] = "Custom"
                self.movingbed["messages"].append(
                    [
                        self.tr("Moving-Bed Test: User modified use to correct settings") + ";",
                        3,
                        6,
                    ]
                )
                guidance_text = self.tr("Provide documentation as to justify the decision to use or not use the test for correction.")
                self.movingbed["guidance"].append(
                    self.guidance_prep(self.movingbed["messages"][-1][0], guidance_text))

        if meas.observed_no_moving_bed:
            self.settings_dict["tab_mbt"] = "Custom"

        if meas.observed_no_moving_bed:
            self.settings_dict["tab_mbt"] = "Custom"

    def check_compass_settings(self, meas):
        """Checks the compass settings for changes.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        self.settings_dict["tab_compass"] = "Default"

        magvar_change = False
        align_change = False

        # Check each checked transect
        for idx in meas.checked_transect_idx:
            transect = meas.transects[idx]

            # Magvar
            if (
                transect.sensors.heading_deg.internal.mag_var_deg
                != transect.sensors.heading_deg.internal.mag_var_orig_deg
            ):
                magvar_change = True

            # Heading offset
            if transect.sensors.heading_deg.external is not None:
                if (
                    transect.sensors.heading_deg.external.align_correction_deg
                    != transect.sensors.heading_deg.external.align_correction_orig_deg
                ):
                    align_change = True

        # Report condition and messages
        if any([magvar_change, align_change]):
            self.settings_dict["tab_compass"] = "Custom"

            if magvar_change:
                self.compass["messages"].append(
                    [self.tr("Compass: User modified magnetic variation") + ";", 3, 4]
                )
                guidance_text = self.tr("If the user has modified the magnetic variation, check that the variation is reasonable for the site.")
                self.compass["guidance"].append(
                    self.guidance_prep(self.compass["messages"][-1][0], guidance_text))

            if align_change:
                self.compass["messages"].append(
                    [self.tr("Compass: User modified heading offset") + ";", 3, 4]
                )
                guidance_text = self.tr("If the user has modified the heading offset, look for collected transects or documentation to justify the heading offset.")
                self.compass["guidance"].append(
                    self.guidance_prep(self.compass["messages"][-1][0], guidance_text))

    def check_oursin(self, meas):
        """Checks the compass settings for changes.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        self.settings_dict["tab_uncertainty_2_advanced"] = "Default"
        self.settings_dict["tab_uncertainty"] = "Default"

        for key in meas.oursin.user_advanced_settings.keys():
            if not np.isnan(meas.oursin.user_advanced_settings[key]):
                self.settings_dict["tab_uncertainty_2_advanced"] = "Custom"
                self.settings_dict["tab_uncertainty"] = "Custom"
                break

        for key in meas.oursin.user_specified_u.keys():
            if not np.isnan(meas.oursin.user_specified_u[key]):
                self.settings_dict["tab_uncertainty"] = "Custom"
                break

    def systest_added(self, meas):
        """Peforms a quality check after a system test has been added.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        self.system_tst_qa(meas)
        self.system_tst["messages"].append(
            [
                self.tr("System Test: A system test has been manually added to the measurement") + ";",
                2,
                3,
            ]
        )
        guidance_text = self.tr("Provide documentation on the source of the system test and why it was not originally collected with this measurement.")
        self.system_tst["guidance"].append(
            self.guidance_prep(self.system_tst["messages"][-1][0], guidance_text))

        self.settings_dict["tab_systest"] = "Custom"

    def compass_added(self, meas):
        """Performs a quality check after a compass calibration / evalution has
        been added.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        self.compass_qa(meas)
        self.compass["messages"].append(
            [
                self.tr("Compass: A compass cal/eval has been manually added to the measurement") + ";",
                2,
                3,
            ]
        )
        guidance_text = self.tr("Provide documentation on the source of the compass calibration and/or evaluatation and why it was not originally collected with this measurement.")
        self.compass["guidance"].append(
            self.guidance_prep(self.compass["messages"][-1][0], guidance_text))

        self.settings_dict["tab_compass"] = "Custom"

    def moving_bed_test_added(self, meas):
        """Performs a quality check after a moving-bed test has
        been added.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        self.moving_bed_qa(meas)
        self.movingbed["messages"].append(
            [
                self.tr("Moving-Bed Test: A moving-bed test has been manually added to the measurement") + ";",
                2,
                3,
            ]
        )
        guidance_text = self.tr("Provide documentation on the source of the moving-bed test and why it was not originally collected with this measurement.")
        self.movingbed["guidance"].append(
            self.guidance_prep(self.movingbed["messages"][-1][0], guidance_text))

        self.settings_dict["tab_mbt"] = "Custom"
