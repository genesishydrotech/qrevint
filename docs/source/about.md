# About QRevInt

**Version:** 1.31

**Date:** 5/30/2024

**Support:** dave@genesishydrotech.com

**Webpage:** [www.genesishydrotech.com](https://www.genesishydrotech.com)

Source Code Repository: [https://bitbucket.org/genesishydrotech/qrevint](https://bitbucket.org/genesishydrotech/qrevint)

## Disclaimer:
This software (QRevInt) is a fork of QRev, which was originally approved for release by the U.S. Geological Survey (USGS), IP-118174. Genesis HydroTech LLC through funding from various international agencies is working to improve and expand the capabilities and features available in QRevInt. While Genesis HydroTech LLC makes every effort to deliver high quality products, Genesis HydroTech LLC does not guarantee that the product is free from defects. QRevInt is provided “as is," and you use the software at your own risk. Genesis HydroTech LLC and contributing agencies make no warranties as to performance, merchantability, fitness for a particular purpose, or any other warranties whether expressed or implied. No oral or written communication from or information provided by Genesis HydroTech LLC or contributing agencies shall create a warranty. Under no circumstances shall Genesis HydroTech LLC or the contributing agencies be liable for direct, indirect, special, incidental, or consequential damages resulting from the use, misuse, or inability to use this software, even if Genesis HydroTech LLC or the contributing agencies have been advised of the possibility of such damages.

## License
Unless otherwise noted, this project is in the public domain in the United States because it contains materials that originally came from the United States Geological Survey, an agency of the United States Department of Interior. For more information, see the official USGS copyright policy at https://www.usgs.gov/information-policies-and-instructions/copyrights-and-credits. Additionally, Genesis HydroTech LLC waives copyright and related rights in the work worldwide through the CC0 1.0 Universal public domain dedication.

QRevInt includes several open-source software packages. These open-source packages are governed by the terms and conditions of the applicable open-source license, and you are bound by the terms and conditions of the applicable open-source license in connection with your use and distribution of the open-source software in this product.  
## Copyright / License - CC0 1.0

The person who associated a work with this deed has dedicated the work to the public domain by waiving all of his or her rights to the work worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law. You can copy, modify, distribute, and perform the work, even for commercial purposes, all without asking permission.

In no way are the patent or trademark rights of any person affected by CC0, nor are the rights that other persons may have in the work or in how the work is used, such as publicity or privacy rights.

Unless expressly stated otherwise, the person who associated a work with this deed makes no warranties about the work, and disclaims liability for all uses of the work, to the fullest extent permitted by applicable law.

When using or citing the work, you should not imply endorsement by the author or the affirmer. 


## Publicity or privacy:

The use of a work free of known copyright restrictions may be otherwise
regulated or limited. The work or its use may be subject to personal data
protection laws, publicity, image, or privacy rights that allow a person to
control how their voice, image or likeness is used, or other restrictions or
limitations under applicable law.

