.. QRev About documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

QRev
=====================================
Overview
--------
QRev was to compute the discharge from a  moving-boat ADCP measurement using
data collected with any of the Teledyne RD Instrument (TRDI), SonTek, or Nortek
bottom tracking ADCPs. QRev improves the consistency and efficiency of
processing streamflow measurements by providing:

* Automated data quality checks with feedback to the user
* Automated data filtering
* Automated application of extrap, LC, and SMBA algorithms
* Consistent processing algorithms independent of the ADCP used to collect the data
* Improved handing of invalid data
* An estimated uncertainty to help guide the user in rating the measurement

Contents:
^^^^^^^^^

The QRev documentation is organized into topics:

* `User Manual <./source/user.html>`_
* `Technical Manual <./source/technical.html>`_
* `Change Log <./source/changelog.html>`_
* `About <./source/about.html>`_

The left navigation pane contains a table of contents where it is possible
to browse through the entire documentation.

The right pane contains a general listing of the current contents for the
particular page which is active, organized
by headings.

There are tools across the top of the main (center) section of the
documentation to enable moving around, exporting
documentation, toggling full screen reading mode, switching between a light
and dark theme, and searching.

.. toctree::
   :hidden:

   ./source/user.md
   ./source/technical.md
   ./source/changelog.md
   ./source/about.md
