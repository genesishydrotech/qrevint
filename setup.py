# -*- coding: utf-8 -*-

# Learn more: https://github.com/kennethreitz/setup.py

from setuptools import setup

with open("README.md") as f:
    readme = f.read()

with open("LICENSE.md") as f:
    license = f.read()

setup(
    name="QRevInt",
    version="1.31.0",
    description="International version of the USGS QRev",
    long_description=readme,
    author="David S. Mueller",
    author_email="dave@genesishydrotech.com",
    url="https://www.genesishydrotech.com",
    license=license,
    REQUIRES_PYTHON=">=3.8.10",
    packages=[
        "qrev",
        "qrev.Classes",
        "qrev.DischargeFunctions",
        "qrev.MiscLibs",
        "qrev.UI",
    ],
    install_requires=[
        "PyInstaller==6.7.0",
        "pyinstaller-hooks-contrib==2024.6",
        "pyinstaller-versionfile==2.1.1",
        "PyQt5",
        "PyQt5-sip",
        "PyQt5-stubs",
        "altgraph==0.16.1",
        "atomicwrites==1.3.0",
        "attrs==19.1.0",
        "click==7.1.2",
        "colorama==0.4.1",
        "cycler==0.10.0",
        "future==0.17.1",
        "importlib-metadata==4.6",
        "macholib==1.11",
        "matplotlib==3.3.3",
        "more-itertools==7.2.0",
        "statsmodels",
        "numpy==1.22.1",
        "numba==0.53.1",
        "pandas==1.4.0",
        "pefile==2022.5.30",
        "pluggy==0.13.0",
        "py==1.8.0",
        "pyparsing==2.4.2",
        "pytest==5.1.3",
        "python-dateutil",
        "python-dotenv==0.10.3",
        "pytz",
        "pywin32-ctypes==0.2.1",
        "reportlab",
        "scipy==1.7.3",
        "scikit-learn==1.3.2",
        "setuptools==42.0.0",
        "sigfig==1.3.3",
        "simplekml",
        "sip",
        "six==1.12.0",
        "utm",
        "wcwidth==0.1.7",
        "xmltodict==0.12.0",
        "zipp==0.6.0",
        "sphinx-markdown-builder",
        "sphinx",
        "myst-parser",
        "sphinx_book_theme",
    ],
)
